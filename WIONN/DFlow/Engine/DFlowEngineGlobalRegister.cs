using System;
namespace DFlowCore
{
	/// <summary>
	/// Stub class for DFlowEngineSimplified,
	/// This class should be addressed by dflows and engines when asking to resolve name
	/// 
	/// For now it resolves node_type_name (string) -> type
	/// 
	/// Prototype, just to make it run.
	/// </summary>
	public static class DFlowEngineGlobalRegister
	{
		static DFlowEngineGlobalRegister(){
			DoLookForNewAssemblies();
		}
	    static  System.Collections.Generic.Dictionary<string, System.Type> nodetypes
                = new System.Collections.Generic.Dictionary<string, System.Type>();	

		
		public static Type QueryNodeType(string name){
			if(!nodetypes.ContainsKey(name)) throw new Exception("Not registered node type!");
			return nodetypes[name]; //note : will throw error if not in
		}
        public static string MakeNodeName(Type tp) {
                //return ((tp.Assembly.GetCustomAttributes(typeof(System.Reflection.AssemblyTitleAttribute), false))[0] as System.Reflection.AssemblyTitleAttribute).Title + "::" + tp.Name;
            return tp.FullName+"@"+tp.Assembly.ManifestModule.Name.Split ('.')[0];
            //return ((tp.Assembly.GetCustomAttributes(typeof(System.Reflection.AssemblyTitleAttribute), false))[0] as System.Reflection.AssemblyTitleAttribute).+ "::" + tp.Name;
        }		
        public static void DoLookForNewAssemblies()
        {
            foreach (System.Reflection.Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                RegisterAssembly(asm);
            }
           
        }		
         public static void RegisterAssembly(System.Reflection.Assembly asm)
        {
            //            System.Reflection.Assembly asm = args.LoadedAssembly;
            try
            {
                foreach (System.Type tp in asm.GetExportedTypes())
                {
					
                    if (tp != null)
                    {
                        try
                        {
                            if (tp.IsSubclassOf(typeof(DFlow.Node))
                                && (!tp.IsGenericTypeDefinition)
                                && (!tp.IsAbstract)
                                && (!tp.IsSubclassOf(typeof(DFlow.__DataShowingControl)))
                                )
                            {
                                string nn=MakeNodeName(tp);
								if (!nodetypes.ContainsKey(nn))
                                {
                                    DFlowCore.Log.Debug("Registering "+nn,0);
                                    nodetypes.Add(nn, tp);
                                }
                            }
                        }
                        catch (System.Exception se)
                        {
                            DFlowCore.Log.Error(se.ToString());
                        }
                    }

                }
            }
            catch (System.Exception se)
            {
                DFlowCore.Log.Error(se.ToString());
            }
        }
		
	}
}

