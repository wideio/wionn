﻿/*
 * DATAFLOWCORE
 * 
Copyright 2012 - Mindstorm Multitouch Limited

Author - Bertrand Nouvel

DataFlowCore is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DataFlowCore is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser Public License for more details.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DFlow
{
    public abstract class __DataShowingControl : DFlow.Node { }

    public abstract class _DataShowingControl<T> : __DataShowingControl
    {

        public T _control;
        public T control
        {
            get
            {
                if (_control == null)
                {
                    _control = InstantiateControl();
                }
                return _control;
            }
            set
            {
                _control = control;
            }
        }
        public abstract object GetData();
        public abstract void SetData(object u);
        public abstract Type GetDataType();
        public abstract T InstantiateControl();
    }

    public abstract class DataShowingControl<U, T> : _DataShowingControl<T>
    {
        [DFlow.InputPin]
        [DFlow.OutputPin]
        public U data;

        public override object GetData()
        {
            return data;
        }
        public override void SetData(object u)
        {
            data = (U)u;
        }
        public override Type GetDataType() { return typeof(U); }
    }


    public class GenericVogonControl<T, U>
    {
        public T framework_control;                         // <- the grid control
        public DFlow._DataShowingControl<U> vogon_control;  // <- the data control
        //public object data;                               // <- data .....   

        public System.Type DisplayedType; //currently displayed type
        //List<Pair<Observer,  GenericVogonControl<T>>> SubObservers = new List<Pair<Observer, ControlWithObservers>>();
    }


}
