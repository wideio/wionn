﻿/*
 * DATAFLOWCORE
 * 
Copyright 2012 - Mindstorm Multitouch Limited

Author - Bertrand Nouvel

DataFlowCore is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DataFlowCore is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser Public License for more details.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DFlow
{
    /// <summary>
    /// Manage different User Interface APIs
    /// </summary>
    public abstract class UIDeclaration
    {
        public UIDeclaration()
        {
            // SO FAR WE EXPECT THAT UI DECLARATION HAPPEN ONLY ONCE PER CODE
            // EVEN IF THERE IS MULTIPLE ENGINE | EVEN IF THERE IS MULTIPLE HEADS
            //
            System.Diagnostics.Debug.Assert(!_Instances.ContainsKey(this.GetType().Name));

            _Instances.Add(this.GetType().Name, this);
        }

        public void Process()
        {

        }

        static Dictionary<string,UIDeclaration> _Instances=new Dictionary<string,UIDeclaration>();

        public static UIDeclaration GetUI(string query) {
            throw new System.NotImplementedException();
        }
    }


}
