﻿/*
 * DATAFLOWCORE
 * 
Copyright 2012 - Mindstorm Multitouch Limited

Author - Bertrand Nouvel

DataFlowCore is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DataFlowCore is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser Public License for more details.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DFlowCore
{
    public static class Time
    {

        static long tstart = System.DateTime.Now.Ticks;        
        static float cnow=0;
        static float cprev = 0;
        public static float time
        {
            get
            {
               // return (((float)(System.DateTime.Now.Ticks-tstart)) / 10000000f);
                return cnow;
            }
        }

        public static float deltat
        {
            get
            {
                return cnow - cprev;
            }
        }


        static public void Tick(bool update_prev=true)
        {
            if (update_prev)
            {
                cprev = cnow;
            }
            cnow= (((float)(System.DateTime.Now.Ticks - tstart)) / 10000000f);
        }

    }
}
