﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DFlowCore.Utils
{
    public class ImplementationStatus
    {
        [System.AttributeUsage(AttributeTargets.Class)]
        public class UnfinishedAttribute : System.Attribute
        {
        };

        [System.AttributeUsage(AttributeTargets.Class)]
        public class UntestedAttribute : System.Attribute
        {
        };

    }
}
