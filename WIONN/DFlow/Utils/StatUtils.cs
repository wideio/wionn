﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DFlowCore
{
    public class StatUtils
    {

        public class MeanValue<T> /* where T : IVectorSpace */
        {
            T v;
            float nv=0;
            public void Add(T cv)
            {
                v =Operator.Add(v, cv);
                nv += 1;
            }

            public T Mean()
            {
                return Operator.MultiplyAlternative(v,1f/nv);
            }
        }

        public class RunningAverageValue<T> /*where T:IVectorSpace*/
        {
            T v;
            float alpha;

            RunningAverageValue(float halflife)
            {
                alpha=Mathf.Pow(0.5f, 1f / halflife);
            }
            

            public void Add(T cv)
            {
                v = Operator.Add(Operator.MultiplyAlternative(v,alpha), Operator.MultiplyAlternative(cv,1-alpha));
            }

            public T Mean()
            {
                return v;
            }
        }

    }
}
