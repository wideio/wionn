using System;
using System.Runtime.InteropServices;

namespace DFlowCore
{
	public class Mathf
	{
		public class Vector2 {
			public float x;
			public float y;
			public Vector2() {x=0;y=0;}
			public Vector2(float _x, float _y) {x=_x;y=_y;}
		}

		public class Vector3 {
			public float x;
			public float y;
			public float z;
			public Vector3() {x=0;y=0;z=0;}
			public Vector3(float _x, float _y, float _z) {x=_x;y=_y;z=_z;}
		}

		public class Vector4 {
			public float x;
			public float y;
			public float z;
			public float w;
			public Vector4() {x=0;y=0;z=0;w=0;}
			public Vector4(float _x, float _y, float _z,float _w) {x=_x;y=_y;z=_z;w=_w;}
		}

		public class Quaternion {
			public float x;
			public float y;
			public float z;
			public float w;
			public Quaternion() {x=0;y=0;z=0;w=0;}
			public Quaternion(float _x, float _y, float _z,float _w) {x=_x;y=_y;z=_z;w=_w;}
		}


		[ DllImport("libm", EntryPoint="fabsf") ]
 		public extern static float Abs(float d);

	    [ DllImport("libm", EntryPoint="acosf") ]
 		public extern static float Acos(float d);
 
 		[ DllImport("libm", EntryPoint="asinf") ]
 		public extern static float Asin(float d);
 
 		[ DllImport("libm", EntryPoint="atanf") ]
 		public extern static float Atan(float d);
 
 		[ DllImport("libm", EntryPoint="atan2f") ]
 		public extern static float Atan2(float y, float x);
 
 		[ DllImport("libm", EntryPoint="cosf") ]
 		public extern static float Cos(float d);
 
 		[ DllImport("libm", EntryPoint="coshf") ]
 		public extern static float Cosh(float d);
 
 		[ DllImport("libm", EntryPoint="expf") ]
 		public extern static float Exp(float d);
 
 		[ DllImport("libm", EntryPoint="logf") ]
 		public extern static float Log(float d);
 
 		[ DllImport("libm", EntryPoint="log10f") ]
 		public extern static float Log10(float d);
 
 		[ DllImport("libm", EntryPoint="powf") ]
 		public extern static float Pow(float x, float y);
 
 		[ DllImport("libm", EntryPoint="sinf") ]
 		public extern static float Sin(float d);
 
 		[ DllImport("libm", EntryPoint="sinhf") ]
 		public extern static float Sinh(float d);
 
 		[ DllImport("libm", EntryPoint="sqrtf") ]
 		public extern static float Sqrt(float d);
 
 		[ DllImport("libm", EntryPoint="tanf") ]
 		public extern static float Tan(float d);
 
 		[ DllImport("libm", EntryPoint="tanhf") ]
 		public extern static float Tanh(float d);
 

	}
}

