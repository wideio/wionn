using System;

using DFlow;
using DFlowCore;

using WIO.ComputingToolkit;
using WIO.Utils;
using WIODataLanguage;
using WIODatasetLanguage;

using System.Linq;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Diagnostics;

using WIO.NN.Obj;

using OpenCL.Net;

using NUnit.Framework;
namespace WIO.NN.Kernels
{
	public static class FinalLayerKernels{
		
		public static void SqrErrorDerivativeMulticlass01(WIOOpenCLMatrix output, 
		                                                         
		                                                         WIOOpenCLMatrix input,
		                                                         
		                                                         WIOOpenCLMatrix correct_classes){

			if(input.GetTransposeState()==1) throw new NotImplementedException();
			if(output.GetTransposeState()==1) throw new NotImplementedException();
			if(correct_classes.GetTransposeState()==1) throw new NotImplementedException();

			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(output);
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(input);	
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(correct_classes);	
		
		  	Cl.ErrorCode error;				
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("sqrErrorDerivativeMulticlass01");

			Assert.AreEqual(output.GetLength(0), input.GetLength(0));
			Assert.AreEqual(output.GetLength(1), input.GetLength(1));
			Assert.AreEqual(correct_classes.GetLength(0), output.GetLength(1));
		
			int intPtrSize = 0;
            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
			IntPtr sizer = new IntPtr(intPtrSize);
			Cl.SetKernelArg(kernel,0, sizer, (output.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,1, sizer, (input.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,2, sizer, (correct_classes.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);

			IntPtr[] dims = new IntPtr[]{ new IntPtr(output.GetLength(0)), new IntPtr(output.GetLength(1)) };
	
            error = Cl.EnqueueNDRangeKernelNullEvent(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, 
			                                dims ,null );
            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
			(output.GetDual () as OpenCLResourceBufferHandle<float>).SetChangeSwitch(true);
		}		
	}
	
	/// <summary>
	/// Holds sigmoid-specific kernels, that cannot be generalized
	/// </summary>
	public static class SigmoidKernels{
	
		/// <summary>
		/// Very specific kernel for accumulating gradient from batch using gradient descent back prop.
		/// In the future we can think how to generalize it nicely
		/// 
		/// It calculates gradient and makes average
		/// 
		/// </summary>
		/// <param name="W">weight matrix</param>
		/// <param name="b">WX+(b)</param>
		/// <param name="a"> a^{l+1} activation </param>
		/// <param name="a_previous"> a^{l} activation </param>
		/// <param name="delta_dash_next">delta_dash^{l+1}, dash stands for not having derivative in it for performance issues</param>
		public static void SigGradientDescentFull(WIOOpenCLMatrix
		                                                  W, WIOOpenCLMatrix b,
		                                                    WIOOpenCLMatrix a_previous,
		                                                    WIOOpenCLMatrix a,
		                                                    WIOOpenCLMatrix delta_dash_next,
		                                                    float learning_epsilon){


			if(W.GetTransposeState()==1) throw new NotImplementedException();
			if(b.GetTransposeState()==1) throw new NotImplementedException();

			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(a);
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(a_previous);
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(delta_dash_next);			
		
		  	Cl.ErrorCode error;				
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("sig_gradient_desc_full");

			Assert.IsTrue(a_previous.GetLength(0)/W.GetLength(1) == delta_dash_next.GetLength(0)/W.GetLength(0));
			Assert.IsTrue(a.GetLength(0) == delta_dash_next.GetLength(0));
			
			Assert.IsTrue(delta_dash_next.GetLength(0) == W.GetLength(0));
			Assert.IsTrue(a_previous.GetLength(0) == W.GetLength(1));
			Assert.IsTrue (delta_dash_next.GetLength(1) == a_previous.GetLength(1));
			
		
			int intPtrSize = 0;
            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
			IntPtr sizer = new IntPtr(intPtrSize);
			Cl.SetKernelArg(kernel,0, sizer, (W.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,1, sizer, (b.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,2, sizer, (a_previous.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,3, sizer, (a.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,4,  sizer,(delta_dash_next.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,5, learning_epsilon);
			Cl.SetKernelArg(kernel,6, delta_dash_next.GetLength(1));

			IntPtr[] dims = new IntPtr[]{ new IntPtr(W.GetLength(0)), new IntPtr(W.GetLength(1)) };
		   // execute kernel
            error = Cl.EnqueueNDRangeKernelNullEvent(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, 
			                                dims ,null );
            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
			
			(W.GetDual () as OpenCLResourceBufferHandle<float>).SetChangeSwitch(true);
			(b.GetDual () as OpenCLResourceBufferHandle<float>).SetChangeSwitch(true);
		}			
		
		
		public static void SigmoidLayerDeltaActivationDerivative(WIOOpenCLMatrix delta_next_dash, WIOOpenCLMatrix activation){

			if(delta_next_dash.GetTransposeState()==1) throw new NotImplementedException();
			if(activation.GetTransposeState()==1) throw new NotImplementedException();

			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(delta_next_dash);
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(activation);		
		
		  	Cl.ErrorCode error;				
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("sigmoidLayerDeltaActivationDerivative");

			Assert.AreEqual(delta_next_dash.GetLength(0), activation.GetLength(0));
			Assert.AreEqual(delta_next_dash.GetLength(1), activation.GetLength(1));
			
		
			int intPtrSize = 0;
            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
			IntPtr sizer = new IntPtr(intPtrSize);
			Cl.SetKernelArg(kernel,0, sizer, (delta_next_dash.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,1, sizer, (activation.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);

			IntPtr[] dims = new IntPtr[]{ new IntPtr(delta_next_dash.GetLength(0)), new IntPtr(delta_next_dash.GetLength(1)) };
	
            error = Cl.EnqueueNDRangeKernelNullEvent(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, 
			                                dims ,null );
            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
			(delta_next_dash.GetDual () as OpenCLResourceBufferHandle<float>).SetChangeSwitch(true);
		}
		
	}
}

