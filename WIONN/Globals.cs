using System;

using WIODataLanguage;
using WIODatasetLanguage;

using OpenCL.Net;
using WIO.Utils;
using WIO.ComputingToolkit;

using System.Drawing;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

using MongoDB.Bson;


using DFlow;
using DFlowCore;

using Emgu;
using Emgu.CV;


using System.Threading;

namespace WIO.NN
{
	public static class Globals{
		// We need multithreading access here
		public static SLogger Logger = new SLogger("WIONN.log");
	}

}

