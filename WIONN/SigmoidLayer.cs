using System;

using DFlow;
using DFlowCore;

using WIO.ComputingToolkit;
using WIO.Utils;
using WIODataLanguage;
using WIODatasetLanguage;

using System.Linq;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;



using WIO.NN.Obj;

using NUnit.Framework;


namespace WIO.NN
{
	public class SigmoidLayerDual : DualLayer{
		
		public static double SigmoidLayerDualProfiler = 0;
		
		[DFlow.InputPin()]
		///this is \delta^{l+1} (notation from UFDL tutorial), repeated column-wisely
		///l counting input layer (it is slightly moved to facililate algebra)
		/// 
		/// note : it is not EXACTLY \delta, it is \delta without f'(z(l)), just little different derivative
		public WIO.NN.Obj.ObjBatchWIOMatrix input;	 

		[DFlow.OutputPin()]
		///this is \delta^{l} (notation from UFDL tutorial), repeated column-wisely
		///l counting input layer (it is slightly moved to facililate algebra)
		/// 
		/// note : it is not EXACTLY \delta, it is \delta without f'(z(l)), just little different derivative
		public  WIO.NN.Obj.ObjBatchWIOMatrix output;	 		
		
		public SigmoidLayerDual(){
			WIO.Utils.ProfilerManager.RegisterTimer("SigmoidLayerDual");
			WIO.Utils.ProfilerManager.RegisterTimer("SigmoidLayerDual-2DCPULoop");
		}
		
		private int _counterGradient = -1;
		
		//gpu computing buffers
		public WIOOpenCLMatrix gradient_buffer;
		public WIOOpenCLMatrix gradient_buffer_acc;
		public override bool IsReady ()
		{
			return this.input.Ready;
		}
			
		public override void Process(){
			WIO.Utils.ProfilerManager.StartProfilerTimer("SigmoidLayerDual");
			
			System.DateTime start = System.DateTime.Now;
			
			SigmoidLayerDual.Parameters p_my = (GetParameters() as Parameters);
			SigmoidLayer dual =(SigmoidLayer)this.Dual;
			SigmoidLayer.Parameters p = (SigmoidLayer.Parameters)dual.GetParameters();	
			

			if((ToolkitBackend)input.parameters["toolkit_backend"] != ToolkitBackend.OPENCL) 
				throw new NotImplementedException("Not implemented backend");
			if((ToolkitBackend)dual.output.parameters["toolkit_backend"] != ToolkitBackend.OPENCL) 
				throw new NotImplementedException("Not implemented backend");			

			//some needed stuff

			WIOOpenCLMatrix dual_activation = (WIOOpenCLMatrix)dual.output.ptr;	
			WIOOpenCLMatrix dual_input_activation = (WIOOpenCLMatrix)dual.input.ptr;	
			WIOOpenCLMatrix dual_W = (WIOOpenCLMatrix)dual.W;
			WIOOpenCLMatrix dual_b = (WIOOpenCLMatrix)dual.b;
			
			//if((int)this.host_Dataflow.GetGlobalVariable("batch_size")==1){  <-- uncomment me		
			if((int)this.host_Dataflow.GetGlobalVariable("batch_size")==0){ //for test
				throw new Exception("Obsolete code");
				
				if(this.gradient_buffer== null){
					this.gradient_buffer = new WIOOpenCLMatrix(dual.W.GetLength(0), dual.W.GetLength(1));
					this.gradient_buffer_acc = new WIOOpenCLMatrix(dual.W.GetLength(0), dual.W.GetLength(1));
					this.gradient_buffer_acc.MApplyLinear(0.0f,0.0f);
				}
				
				//this is different in one by one processing for one. I will keep it this way
				//because it is obsolete code anyway
				WIOOpenCLMatrix delta_l_next = (WIOOpenCLMatrix)input.ptr; //delta rom the next layer
				WIOOpenCLMatrix delta_l = (WIOOpenCLMatrix)output.ptr;
				
				
				Assert.IsTrue (delta_l_next.GetLength(0) == dual_activation.GetLength(0));
				Assert.IsTrue (delta_l.GetLength(0) == dual_input_activation.GetLength(0));
				
				
				//calculate delta.l (this.output_ptr)
				
				//make it a kernel
				if(p.IsOutputLayer){
					//delta from the next layer is just DE\Dz
					//FIXME: looping not to call GPU, too big overhead (fix?)
					for(int i=1;i<=delta_l_next.GetLength(0);++i){
						delta_l_next[i,1] *=
							(GetParameters() as Parameters).Epsilon*dual_activation[i,1] * (1-dual_activation[i,1]);
					}
				}
	
			
				//TODO: optimize it as one kernel, no time for it now so prototyping
				
				
				
				
				
				dual_W.Transpose();
				dual_W.Multiply(delta_l_next,_self:false, dest:delta_l);
				dual_W.Transpose();
				delta_l.Update();
				
				for(int i=1;i<=delta_l.GetLength(0);++i){
					delta_l[i,1] *=(GetParameters() as Parameters).Epsilon*
						dual_input_activation[i,1] * (1-dual_input_activation[i,1]);
				}				
		
	
				//matrixop
	//			dual_activation.Update();
				dual_input_activation.Transpose();
				delta_l_next.Multiply(dual_input_activation,_self : false, dest : this.gradient_buffer);
				dual_input_activation.Transpose();	
				_counterGradient ++;
				this.gradient_buffer_acc.Add (this.gradient_buffer,_self : true);
				gradient_buffer_acc.Update();
	
				//accumualte and zero accumulator
				if(_counterGradient == (GetParameters() as Parameters).GradientComposeAverage){
					this.gradient_buffer_acc.MApplyLinear(
						 1.0f/
						(GetParameters() as Parameters).GradientComposeAverage,0.0f);
					
					//this should be scaled already
					dual.W.Substract(this.gradient_buffer_acc, _self: true);
					//add here the loop because it will be anyway the last time we use delta_l_next
					dual.b.Substract(delta_l_next);	
					(dual.W as WIOOpenCLMatrix).Update();
					
					this.gradient_buffer_acc.MApplyLinear(0.0f,0.0f);
					_counterGradient = 0;
				}	
				
			}
			else{
				//Batched procedure, different handling (GradientComposeAverage becomes not used and uses
				//heavily kernel)
				WIOOpenCLMatrix delta_l_next_dash  = (WIOOpenCLMatrix)input.ptr; //delta rom the next layer
	
				
				
				WIOOpenCLMatrix delta_l_dash = (WIOOpenCLMatrix)output.ptr;				

				if(delta_l_dash == null) Console.WriteLine("Here is the problem");
				if(dual_activation == null) Console.WriteLine("Here is the problem 2");
				//now in gradient_buffer_acc I have what needed
				Assert.AreEqual(delta_l_next_dash.GetLength(0), dual_activation.GetLength(0));
				Assert.AreEqual(delta_l_next_dash.GetLength(1), dual_activation.GetLength(1));
				

				
	
				
				WIO.Utils.ProfilerManager.StartProfilerTimer("SigmoidLayerDual-2DCPULoop");
				WIOOpenCLMatrix delta_l_next = delta_l_next_dash; //indicate change.
				

//				
//				for(int i=1;i<=delta_l_next.GetLength(0);++i) 
//					for(int j=1;j<=delta_l_next.GetLength(1);++j)
//						delta_l_next[i,j]*=dual_activation[i,j]*(1.0f-dual_activation[i,j]);
//				
				Kernels.SigmoidKernels.SigmoidLayerDeltaActivationDerivative(delta_l_next_dash, dual_activation);
//				for(int i=1;i<=delta_l_dash.GetLength(0);++i) 
//					for(int j=1;j<=delta_l_dash.GetLength(1);++j)	{
//						float sum =0.0f;
//						for(int k=1;k<=dual_W.GetLength(0);++k) sum+=dual_W[k,i]*delta_l_next[k,j];
//						delta_l_dash[i,j] = sum;
//					  }
//				
				WIO.Utils.ProfilerManager.StopProfilerTimer("SigmoidLayerDual-2DCPULoop");
				//TODO: push it into kernel
				
				
				//replace this one as well?
				dual_W.Transpose();
				dual_W.Multiply(delta_l_next,_self:false, dest:delta_l_dash);
				dual_W.Transpose();	

				
				
				
//				for(int i=1;i<=dual_W.GetLength(0);++i){
//				
//					for(int j=1;j<=dual_W.GetLength(1);++j){
//					
//						float sum = 0.0f;
//						for(int b=1;b<=delta_l_next.GetLength(1);++b){
//							sum+= delta_l_next[i,b] * dual_input_activation[j,b];
//						}
//						sum/=delta_l_next.GetLength(1);
//						sum*=(GetParameters() as Parameters).Epsilon;
//						
//						dual_W[i,j]-=sum;
//						
//
//						
//					}	
//					float sum_b = 0.0f;
//					for(int b=1;b<=delta_l_next.GetLength(1);++b){
//						sum_b+= delta_l_next[i,b];
//					}
//					sum_b/=delta_l_next.GetLength(1);
//					sum_b*=(GetParameters() as Parameters).Epsilon;
//					dual_b[i,1] -= sum_b;				
//				
//				}
				
			
				WIO.NN.Kernels.SigmoidKernels.SigGradientDescentFull(
					dual_W,dual_b,dual_input_activation,dual_activation,delta_l_next,
					(GetParameters() as Parameters).Epsilon);	
			}
			WIO.Utils.ProfilerManager.StopProfilerTimer("SigmoidLayerDual");
			this.input.Ready = false;
			this.output.Ready = true;
		}
		
		public override void UpdateParameters (object o)
		{
			base.UpdateParameters(o);
			
			Parameters casted_parameters = (Parameters)o;
				
			this.parameters = casted_parameters;	

			SigmoidLayerDual.Parameters p = (SigmoidLayerDual.Parameters)GetParameters();	
			

			
			//note: I am not initializing buffers here, because it is parametrized by dflow. Dflow should do it
			//for now it is _precheck_conditions in NeuralNetworks.cs
			
			//input doesn't get initialized at all, dual layers will always accept the same thing they are sent for now.
			//so not need for buffer's (that's what intializing input pins might be for as in SigmoidLayer)
			this.output = new ObjBatchWIOMatrix(WIO.NN.ToolkitBackend.OPENCL,p.N );
			this.input = new ObjBatchWIOMatrix( WIO.NN.ToolkitBackend.OPENCL,p.K);
		}
		
		public new class Parameters : DualLayer.Parameters{
			/// <summary>
			/// How often should this layer change weights?
			/// </summary>
			public int GradientComposeAverage =1 ; 
			
			/// <summary>
			/// Learning parameter for trivial gradient descent
			/// </summary>
			public float Epsilon = 0.01f;
			
			public int N;
			public int K;
				
		
		}
		
		public override string GetDescriptor ()
		{
			return "SigmoidLayerDual";
		}	
		
	}
	
	public class SigmoidLayer : Layer{
		public static double SigmoidLayerProfiler = 0;
		
		
		#region PINS	
		[DFlow.InputPin()]
		public  WIO.NN.Obj.ObjBatchWIOMatrix input;
		[DFlow.OutputPin()]
		public  WIO.NN.Obj.ObjBatchWIOMatrix output;
		#endregion PINS
		
		public SigmoidLayer(){
			WIO.Utils.ProfilerManager.RegisterTimer("SigmoidLayer");
		}
	
		
		/// <summary>
		/// The weight matrix. Note that notation is slightly different than 
		/// UFDL's . W = W^{l-1}, the layer holds here W of layer before as if.
		/// It is for convenience, i didn't want to make input layer hold W matrix and
		/// make it independent of NN ahead
		/// </summary>
		public WIOMatrix W,b;
		public WIOMatrix current_input;
	
		
		public new class Parameters : Layer.Parameters{
			/// <summary>
			/// Previous layer size
			/// </summary>
			public int N; 
			/// <summary>
			/// Current layer size
			/// </summary>
			public int K; 
		
			/// <summary>
			/// If it is output layer, it triggers different delta calculation
			/// </summary>
			public bool IsOutputLayer = false;
			
			/// <summary>
			/// The w_rnd_min controls range in which random activation takes place.
			/// There are some theoretical results about appropriate ranges
			/// But who cares for now
			/// </summary>
			public float w_rnd_min = 0.0f, w_rnd_max = 0.0f;
			
			
							

			
			/// <summary>
			/// Is this layer the first layer ? (no delta_l calculated)
			/// </summary>
			public bool IsFirst = false;	
		}
		
	
		
		public override void UpdateParameters(object p){
			base.UpdateParameters(p);
			
			Parameters casted_parameters = (Parameters)p;
			
			Globals.Logger.Log("Updating parameters in SigmoidLayer");
			
			this.parameters = p;
			
			if(casted_parameters.backend != ToolkitBackend.OPENCL) throw new NotImplementedException("Not implemented backend");
			
			
			if(casted_parameters.w_rnd_max==casted_parameters.w_rnd_min){
				casted_parameters.w_rnd_min = (float)-Math.Sqrt (12.0/(casted_parameters.K+casted_parameters.N));
				casted_parameters.w_rnd_max = (float)Math.Sqrt (12.0/(casted_parameters.K+casted_parameters.N));
			}
			
			

			float[,] rndInitW = new float[casted_parameters.K, casted_parameters.N];
			float[,] rndInitB = new float[casted_parameters.K, 1];
			System.Random rnd = new System.Random((int)(WIO.ComputingToolkit.Utils.GetCurrentTimestamp()%100));
			
			
			
			
			for(int i=0;i<casted_parameters.K;++i){
				rndInitB[i,0]=casted_parameters.w_rnd_min + 
						(casted_parameters.w_rnd_max - casted_parameters.w_rnd_min)*(float)rnd.NextDouble();
				for(int j=0;j<casted_parameters.N;++j)
					rndInitW[i,j]=casted_parameters.w_rnd_min + 
						(casted_parameters.w_rnd_max - casted_parameters.w_rnd_min)*(float)rnd.NextDouble();
			}
			
			this.W = new WIOOpenCLMatrix(rndInitW);
			this.b = new WIOOpenCLMatrix(rndInitB);

			
			//note: I am not initializing buffers here, because it is parametrized by dflow. Dflow should do it
			//for now it is _precheck_conditions in NeuralNetworks.cs
			
			this.output = new WIO.NN.Obj.ObjBatchWIOMatrix(casted_parameters.backend,casted_parameters.K ) ;   			
			this.input = new WIO.NN.Obj.ObjBatchWIOMatrix(casted_parameters.backend,casted_parameters.N);
		}
		

		public override string GetDescriptor ()
		{
			Parameters casted_parameters = (Parameters)GetParameters();
			return "SigmoidLayer"+ casted_parameters.N.ToString()+";"+casted_parameters.K.ToString();
		}
		
		
		public override void Process ()
		{	
			WIO.Utils.ProfilerManager.StartProfilerTimer("SigmoidLayer");
			
			
			if((ToolkitBackend)this.input.parameters["toolkit_backend"] != ToolkitBackend.OPENCL) throw new
				NotImplementedException("Not implemented backend");
			if((ToolkitBackend)this.output.parameters["toolkit_backend"] != ToolkitBackend.OPENCL) throw new
				NotImplementedException("Not implemented backend");
			
			
			
            WIOOpenCLMatrix input_matrix = (WIOOpenCLMatrix)this.input.ptr;
			WIOOpenCLMatrix output_matrix = (WIOOpenCLMatrix)this.output.ptr;
			Assert.AreEqual(input_matrix.GetLength(0), ((Parameters)this.parameters).N);

				
//			GPUResourceManager.GetFLOATBUFValue();			
			this.W.Multiply(input_matrix, _self:false, dest: output_matrix); //W*X auto-broadcasting of W				
//			GPUResourceManager.GetFLOATBUFValue();
			output_matrix.BatchAdd (this.b, _self:true); //W*X  + B, broadcasting B..
			output_matrix.MApplySigmoid(); //sigmoid(WX + B)
		

			this.input.Ready = false;
			this.output.Ready = true;

			
			WIO.Utils.ProfilerManager.StopProfilerTimer("SigmoidLayer");
			
		}
		
		public override void LoadPersistentData (object data)
		{
			Dictionary<string, object>  persistent_data = (Dictionary<string,object>)(data);
			Parameters casted_parameters = GetParameters() as Parameters;	
			
			((WIOOpenCLMatrix)this.W).RewriteBuffer((float[])persistent_data["W"],casted_parameters.K, casted_parameters.N);
			((WIOOpenCLMatrix)this.b).RewriteBuffer((float[])persistent_data["b"],casted_parameters.K,1);
			
		}
		public override object GetPersistentData ()
		{
			Dictionary<string, object> construct = new Dictionary<string, object>();
			construct["W"] = ((WIOOpenCLMatrix)this.W)._dumpToManagedFloatArray();
			construct["b"] = ((WIOOpenCLMatrix)this.b)._dumpToManagedFloatArray();
			return construct;
		}
		
		public override bool IsReady ()
		{
			//facililates control of blocking flow when doing batching etc in the input layer
			return this.input.Ready;
		}
	
	}

}

