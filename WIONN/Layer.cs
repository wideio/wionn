using System;

using DFlow;
using DFlowCore;

using WIO.ComputingToolkit;
using WIO.Utils;
using WIODataLanguage;
using WIODatasetLanguage;

using System.Linq;
using System.Drawing;
using System.Reflection;

using WIO.NN.Obj;

using NUnit.Framework;
namespace WIO.NN
{
	

	
	/// <summary>
	/// Generic class for layer concept in NN
	/// </summary>
	public abstract class Layer : DFlow.Node{
		[DFlow.InputPin(), DFlow.OutputPin()]
		public ObjMeta meta;
		
		
		/// <summary>
		/// The layer format
		/// </summary>
		public LayerInputFormat LayerFormat;
	
		
		/// <summary>
		/// Gets the persistent data. It will be saved in array by neural network model, which names of each layer etc.
		/// So in essence NN node will query its layers to get persistent data. And if created again with the same architecture
		/// it will load them by SetPersistentData()
		/// 
		/// The design is that GetPersistendData() computes in most cases the data to be dumped. Bookkeeping it 
		/// during training and using node is pointless
		/// </summary>
		public virtual object GetPersistentData(){
			return "empty";
		}
		
		/// <summary>
		/// Dumps the descriptor. Idea is that concatenation of descriptors should provide
		/// unique and 1 to 1 description of used architecture. It allows for good
		/// caching system
		/// </summary>
		public virtual string GetDescriptor(){
			return "Layer";
		}
		
		/// <summary>
		/// Loads persistent data - so we do not start from scratch
		/// </summary>
		public virtual void LoadPersistentData(object data){
			return;
		}
		
		
		
		
		
		
		
		public virtual void Init(){
			Globals.Logger.Log("Initializing " + this.GetType().Name);
		}
		
		public Layer(){
			meta = new ObjMeta();
		}
		
		/// <summary>
		/// After calling this funciton no previous iterations should have impact on this
		/// iteration (of course leaving off persistent things like weights)
		/// </summary>
		public virtual void Reset(){
			return;
		}
		
		
		public class Parameters{
			/// <summary>
			/// Used backend
			/// </summary>
			public ToolkitBackend backend = ToolkitBackend.OPENCL;	
			/// <summary>
			/// The cached data - it will be used only in the UpdateParameters function and
			/// will load things that are memorized from cache (weight matrix etc)
			/// </summary>
			public object CachedData; 
		}
	}
	
	public abstract class FinalLayer : Layer{
		/// <summary>
		/// Should layer perform classification?
		/// </summary>
		public bool ClassificationPhase = false;
		/// <summary>
		/// This output will be something that goes out of the whole TrainableNode, like
		/// classification for instance
		/// </summary>
		[DFlow.OutputPin()]
		public WIONNObj final_output;
		
		public FinalLayer(){
			Globals.Logger.Log ("Sanity check that it got called");
			final_output = new WIONNObj();
		}
	}
	
	public abstract class DualLayer : Layer{
		public new class Parameters : Layer.Parameters{
			public int dual_index = -1; //some way of referring to dual, it is a prototype
		} 
		private bool _flow = true;
		public override bool IsReady ()
		{
			return this.GetFlowStatus();	
		}
		
		public Layer Dual;
		
		public virtual bool GetFlowStatus(){
			return this._flow;
		}
		/// <summary>
		/// Disable or enable node - should always send is not read
		/// </summary>
		public virtual void SetFlow(bool value){
			this._flow = value;
		}

	}
}

