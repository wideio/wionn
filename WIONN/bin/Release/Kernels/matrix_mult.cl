__kernel void
floatMatrixMult(     __global       float * MResp,
                     const __global       float * M1,
                     const __global       float * M2, 
                     const int tran1, 
                     const int tran2,
                    const int common_side,
                    const int rows_dev_M1,
                    const int rows_dev_M2                     )
{
    // Vector element index
    int i = get_global_id(0);
    int j = get_global_id(1);

    int p = get_global_size(0);
    int r = get_global_size(1);

    int index_dst = get_index2(i,j,p,0);
    MResp[index_dst] = 0;

    int index1, index2;

    for (int k = 0; k < common_side ; k++)
    {
        index1 = get_index2(i,k,rows_dev_M1,tran1); 
        index2 = get_index2(k,j,rows_dev_M2,tran2);
        MResp[index_dst] += M1[index1] * M2[index2];
    }
} 
