__kernel void
floatMatrixAddSelf(  __global       float * M1,
                     const __global       float * M2,
                    int tran1, int tran2, int colsM2)
{
    int i = get_global_id(0);
    int j = get_global_id(1);

    int p = get_global_size(0);
    int q = get_global_size(1);

    int index1 = get_index2full(i,j,p,q,tran1);
    int index2 = get_index2full(i,j%colsM2,p,colsM2,tran2);

     M1[index1] += M2[index2];
} 
