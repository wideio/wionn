__kernel void
matrixSetElementKernel( __global float * M,
                         int row, int col, int rows_dev, int tran, float val)
{
    int index = get_index2(row,col,rows_dev,tran);
    M[index] = val;
}

