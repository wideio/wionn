//it is highly specialized kernel, should be generalized later (theano like)
//in fact it is an element wise function with parameters : should be easy to generalize
__kernel void
sqrErrorDerivativeMulticlass01( __global float * output, const __global float * input, const __global int * correct_classes)
{
    int i = get_global_id(0);
    int j = get_global_id(1);

    int p = get_global_size(0);
    int q = get_global_size(1);

	int indx = get_index2full(i,j,p,q,0);

	output[indx] = -( (correct_classes[j] == i ? 1.0f : 0.0f) - input[indx]);	
} 
