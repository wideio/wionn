//it is highly specialized kernel, should be generalized later (theano like)
//in fact it is an element wise function : should be easy to generalize
__kernel void
sigmoidLayerDeltaActivationDerivative( __global float * delta_next_dash, const __global float * activation)
{
    int i = get_global_id(0);
    int j = get_global_id(1);

    int p = get_global_size(0);
    int q = get_global_size(1);

	int indx = get_index2full(i,j,p,q,0);

	delta_next_dash[indx] *= activation[indx]*(1.0f - activation[indx]);
} 
