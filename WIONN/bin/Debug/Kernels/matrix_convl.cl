//we assume that filter has to be a square
//i treat everything 
__kernel void Convolve(
	const __global float * pInput, 
	const __global float * pMask,
	__global float * pOutput, 
	const int offsetRow, const int offsetColumn,
	const int nInRows ,
	const int nInColumns,
    const int nInTran,
	const int nMaskWidth
) { 
		
		const int nRows = get_global_size(0);
		const int nColumns = get_global_size(1);


		const int rOut = get_global_id(0);
		const int cOut = get_global_id(1);


		const int rInTopLeft = rOut; //it is this way  
		const int cInTopLeft = cOut;



		float sum = 0; 
		for (int c = 0; c < nMaskWidth; c++) {  //this is column index
			for (int r = 0; r < nMaskWidth; r++) { 
				const int idxF = get_index2(r,c,nMaskWidth,0); 
				const int idxIn = get_index2full((rInTopLeft+r),(cInTopLeft + c),nInRows, nInColumns, nInTran);

				sum += pMask[idxF]*pInput[idxIn]; 
			} 
		} 

		const int idxOut = get_index2(rOut, cOut, nRows, 0);
		pOutput[idxOut] = sum; 

} 
