__kernel void
matrixGetElementKernel( const __global float * M,
                        unsigned int row, unsigned int col, unsigned int rows_dev, int tran, __global float * buffer)
{
    int index = get_index2(row,col,rows_dev,tran);
    buffer[0] = M[index];
}

