__kernel void mapply_linear_tran00(const __global float * M, float a, float b, __global float * dest)
{
	dest[get_global_id(1)*get_global_size(0) + get_global_id(0)] = M[get_global_id(1)*get_global_size(0) + get_global_id(0)]*a + b;
}
