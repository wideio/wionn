//specific sigmoid backprop kernel
//it will be generalized in the future
__kernel void
SigGradientDescent(__global float * W,
__global float * b, 
const __global float * a_prev, 
const __global float * a, 
const __global float * delta_dash_next,
const float epsilon ,
const int delta_dash_next_length,
const int a_length,
__global float * W,
__global float * b
	)
{
    // Vector element index
    int i = get_global_id(0);
    int j = get_global_id(1);

    int p = get_global_size(0);
    int r = get_global_size(1);

	int batches = delta_dash_next_length / p; //check in host code if it equals to a_length/r, should!

	//Calculate W
	for(int b=0;b<batches;++b)
		W[get_index2(i,j,p,0)] -= (epsilon/batches)*(delta_dash_next[i+b*p]*a[i+b*p]*(1.0f - a[i+b*p]))*a_prev[j+b*r];

	//Change b
	for(int b=0;b<baches;++b)
		b[i] -= (epsilon/batches)*(delta_dash_next[i+b*p]*a[i+b*p]*(1.0f - a[i+b*p]));
	
}
