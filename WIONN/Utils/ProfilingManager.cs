using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace WIO.Utils
{
		/// <summary>
		/// Profiler manager. Note : to make it efficient it does no
		/// assumpion/assert checks etc.	
		/// </summary>
		public static class ProfilerManager{
		
			public static void RegisterTimer(object obj){
				Timers[obj] = 0.0;
				Markers[obj] = 0.0;
			}
			public static void ResetTimer(object obj){
				Timers[obj] = 0.0;
				Markers[obj] = 0.0;
			}		
			/// <summary>
			/// Starts the profiler timer. It is associated with
			/// object, can be type of class, or single object
			/// </summary>
			public static void StartProfilerTimer(object obj){
				if(!Timers.ContainsKey(obj)) RegisterTimer (obj);
				Starts[obj] = System.DateTime.Now;
			}
			public static void StopProfilerTimer(object obj){
				Timers[obj]+= (System.DateTime.Now - Starts[obj]).TotalMilliseconds;
			}
			
			/// <summary>
			/// Gets the total time accumulated in timer for this object
			/// </summary>
			public static double GetTotalTime(object obj){ return Timers[obj];}
			/// <summary>
			/// Gets the time to accumulated to mark
			/// </summary>
			public static double GetTimeToMark(object obj){return Timers[obj] - Markers[obj];}
			
			/// <summary>
			/// Sets mark for profiler (so it will count from zero from this mark)
			/// </summary>
			public static void SetMark(object obj){ Markers[obj] = Timers[obj]; }
		
			public static IEnumerable<Tuple<object, double>> DumpMarkedTimes(){
				return Timers.Keys.Select (k => 
			                           
			                           new Tuple<object, double>(k,
			                          Timers[k] -Markers[k]))
			;
			}
			public static IEnumerable<Tuple<object, double>> DumpTimes(){
				return Timers.Keys.Select (k => 
			                           
			                           new Tuple<object, double>(k,
			                           Timers[k]))
				;
		}		
		
			
			private static Dictionary<object,System.DateTime> Starts = new Dictionary<object, System.DateTime>();
			private static Dictionary<object,double> Timers = new Dictionary<object, double>();
			private static Dictionary<object, double> Markers = new Dictionary<object, double>();
		}
	
}

