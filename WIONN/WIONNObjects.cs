using System;
using OpenCL;
using OpenCL.Net;


using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Diagnostics;

using NUnit.Framework;

using System.IO;


using WIO.ComputingToolkit;
using WIO.Utils;

namespace WIO.NN.Obj
{
	
	
	#region WIONNObjs
	
	/// <summary>
	/// Wrapper for object that will denote its format. It will allow for more intelligent conversions
	/// There wrappers for all types, because conversion might much more complex than just between types.
	/// For instance converting bitmap for GPU computing might be a hard task, and will heavily depend
	/// on next layer, etc.
	/// </summary>
	public class WIONNObj{
		/// <summary>
		/// The parameters - should replace object[] parameters. TODO
		/// </summary>
		public Dictionary<string, object> parameters = new Dictionary<string, object>();
		/// <summary>
		/// The pointer to the object
		/// </summary>
		public object ptr;
		/// <summary>
		/// Is this already well-constructed
		/// </summary>
		public bool Ready = false;

		public WIONNObj(object ptr = null){
			this.ptr = ptr;
		}
	}
	
	public class ObjBitmap : WIONNObj{
		public ObjBitmap(object ptr = null) : base(ptr){
		}
	}
	public class ObjMeta : WIONNObj{
		public ObjMeta(object ptr = null): base(ptr){
		}	
	}
	/// <summary>
	/// 1 single example that we might
	/// </summary>
	public class Obj1DWIOVect : WIONNObj{
		public Obj1DWIOVect(ToolkitBackend toolkit_backend = ToolkitBackend.OPENCL,
		                         object ptr = null): 
				base(ptr)
		{
			this.parameters["toolkit_backend"] = toolkit_backend;
		}		
	}
	public class ObjBitmapArray : WIONNObj{
		public ObjBitmapArray(object ptr = null) : base(ptr){
			
		}
	}	
	public class ObjBatchWIOMatrix : WIONNObj{
		/// <summary>
		/// Initializes a new instance of the ObjMatchWIOMatrix
		/// </summary>
		/// <param name='N'>
		/// N - number of rows - it won't change regardless of batc hsize
		/// </param>
		/// <param name='toolkit_backend'>
		/// Used backend
		/// </param>
		/// <param name='ptr'>
		/// Ptr to object, can be null
		/// </param>
		public ObjBatchWIOMatrix(ToolkitBackend toolkit_backend,int N,
		                         object ptr = null): 
				base(ptr)
		{
			this.parameters["rows"] = N;
			this.parameters["toolkit_backend"] = toolkit_backend;
		}		
	}	
	
	/// <summary>
	/// IntPtr to block of code
	/// </summary>
	public class ObjBitmapUnmanaged1D: WIONNObj{
		/// <summary>
		/// Initializes a new instance of the <see cref="WIO.NN.Obj.ObjBitmapUnmanaged1D"/> class.
		/// </summary>
		/// <param name='toolkit_backend'>
		/// Toolkit_backend.
		/// </param>
		/// <param name='Width'>
		/// Width.
		/// </param>
		/// <param name='Height'>
		/// Height.
		/// </param>
		/// <param name='type'>
		/// Type- note : it is string for easier serialization.
		/// </param>
		/// <param name='ptr'>
		/// Ptr.
		/// </param>
		public ObjBitmapUnmanaged1D(ToolkitBackend toolkit_backend,
		        int Width, int Height, string type = "float", object ptr = null): base(ptr){
			
			this.parameters["toolkit_backend"] = toolkit_backend;
			this.parameters["Width"] = Width;
			this.parameters["Height"] = Height;
			this.parameters["type"] = type;
			
		}
	}
	/// <summary>
	/// In fact this is yet another array of dimensionality N*K;
	/// </summary>
	public class ObjBitmapUnmanagedBatch1D : WIONNObj{
		/// <summary>
		/// This is simply a batch (a contatenation) of unmanaged 1d vectors that are bitmaps
		/// </summary>
		/// <param name='Width'>
		/// Width  of image
		/// </param>
		/// <param name='Height'>
		/// Height of image
		/// </param>
		/// <param name="N">
		/// Number of images
		/// </param>
		/// <param name='ptr'>
		/// Pointer (IntPtr)
		/// </param>
		/// <param name="type">
		/// Type- note : it is string for easier serialization.
		/// <param>
		/// 
		public ObjBitmapUnmanagedBatch1D(ToolkitBackend toolkit_backend,
		         int Width, int Height, int N,string type = "float",object ptr = null) : base(ptr){
			this.parameters["toolkit_backend"] = toolkit_backend;
			this.parameters["Width"] = Width;
			this.parameters["Height"] = Height;
			this.parameters["type"] = type;
			this.parameters["N"] = N;
		}
	}		
	#endregion WIONNObjs
}

