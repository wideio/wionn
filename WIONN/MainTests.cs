using System;

using WIODataLanguage;
using WIODatasetLanguage;

using OpenCL.Net;
using WIO.Utils;
using WIO.ComputingToolkit;

using System.Drawing;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

using MongoDB.Bson;


using DFlow;
using DFlowCore;

using Emgu;
using Emgu.CV;

using System.Threading;

/// <summary>
/// Idea : move dataflow as a subdataflow.
/// </summary>

namespace WIO.NN
{
	public class MainClass
	{
		
		/// <summary>
		/// Prepares training and testing datasets using WIODatasetLanguage constructs.
		/// </summary>
		public static Tuple<IDataset, IDataset> PrepareGenDataset(){
			GenLetterDataset glg = new GenLetterDataset();
			glg.UpdateParameters(new GenLetterDataset.Parameters
			                     {
				fontname = "Ubuntu", 
				sz = 8}
			);
			
			glg.printFontList();

			Console.WriteLine("Size of glg {0}", glg.Count);
			
			int width=0, height=05;
			foreach(object id in glg.Keys()){
			
				System.Drawing.Image img= (System.Drawing.Image)glg.GetElement(id);
			
				width = img.Width;
				height = img.Height;
				
				break;
			}
			
			ImageFilterStarDataset ifsd2 = new ImageFilterStarDataset();
			ifsd2.UpdateParameters(new ImageFilterStarDataset.Parameters{
				dataset = glg,
				count = 18,
				filter_params = new object[]{-20.0,20.0,255.0,255.0,255.0,255.0,255.0,255.0},
				filter = ImageFilter.FILTER_ROTATION
			});
			
			ImageFilterStarDataset ifsd = new ImageFilterStarDataset();
			ifsd.UpdateParameters(new ImageFilterStarDataset.Parameters{
				dataset = ifsd2,
				count =10,
				filter_params = new object[]{0.9,1.1},
				filter = ImageFilter.FILTER_BRIGHTNESS	
			});
			
			ImageFilterDataset ifsd3 = new ImageFilterDataset();
			ifsd3.UpdateParameters(new ImageFilterDataset.Parameters{
				dataset = ifsd,
				filter_params = new object[]{width,height},
				filter = ImageFilter.FILTER_RESIZE	
			});	
			

			ShuffleDataset sd = new ShuffleDataset();
			sd.UpdateParameters( new ShuffleDataset.Parameters{dataset = ifsd3, seed = (int)(WIO.ComputingToolkit.Utils.GetCurrentTimestamp()%1000)});
			
			TrainingDataset td = new TrainingDataset();
			td.UpdateParameters(new TrainingDataset.Parameters{removeItems = false, seed = 6,
				training_percentage = 0.85, source_dataset = sd});
			
			
			ExcludeDataset ed = new ExcludeDataset();
			ed.UpdateParameters(new ExcludeDataset.Parameters{dataset=sd, exdataset = td, removeItems = false});
			
			Globals.Logger.Log ("Created "+td.Count.ToString()+" training dataset and " + ed.Count.ToString() + " test dataset");
			
			
			return new Tuple<IDataset,IDataset>(td,ed);
		}

		//Obsolete
//		public delegate Tuple<object,object> prepareDatasetDelegate(); 
//		public static void RunMLP(int classes, prepareDatasetDelegate dataset_generator, 
//		                          int batch_size, int hiddent_layer = 110, float epsilon = 0.01f, int input_width = 28, int input_height=28){
//		
//			int input_size = input_width * input_height;
//			
//			/*Prepare dataset and separate to training and test subsets*/
//			Tuple<object, object> prep = dataset_generator();
//			
//			
//			IDataset training_dataset=null,test_dataset= null ;
//			string training_dataset_source="", test_dataset_source="";
//			if(prep.Item1 is IDataset && prep.Item2 is IDataset){
//				training_dataset = (IDataset)prep.Item1; 
//				test_dataset = (IDataset)prep.Item2;
//			}else if(prep.Item1 is String && prep.Item2 is String){
//				training_dataset_source = (string) prep.Item1; 
//				test_dataset_source = (string)prep.Item2;
//				
//				
//				
//			}
//			else{
//				throw new Exception("Wrong dataset generator");
//			}
//
//			int cnt = 0;
//			if(cnt == 1) foreach(object id in training_dataset.Keys()){ WIO.ComputingToolkit.Utils.DisplayOpenCLImage(
//				new System.Drawing.Bitmap((Image)training_dataset.GetElement(id)),"ShowImage",100); if(++cnt >50) break; }
//			
////			/*Run everything*/
////			RunLogisticRegressionTrain(prep.Item1, prep.Item2);
//			
//			
//			
////			
//			/* Initialize layer architecture */
//			BatchingImagesOptimizedInputLayer inputLayer = new BatchingImagesOptimizedInputLayer();
//			SigmoidLayer sigmoidLayer1 = new SigmoidLayer();
//			SigmoidLayer sigmoidLayer2 = new SigmoidLayer();
//			MultiClassBestSqrErrorLayer classifierLayer = new MultiClassBestSqrErrorLayer();
//			SigmoidLayerDual sigmoidLayerDual1 = new SigmoidLayerDual();
//			SigmoidLayerDual sigmoidLayerDual2 = new SigmoidLayerDual();
//			
//			
//			
//			
//			/*Set parameters (note no need to set next,prev. It will b set in parameters)*/
//			inputLayer.UpdateParameters(
//				new BatchingImagesOptimizedInputLayer.Parameters{
//				Width = input_width,
//				Height = input_height
//				
//			}
//			);
//			sigmoidLayer1.UpdateParameters(
//				new SigmoidLayer.Parameters{N = input_size, K = hiddent_layer}
//			);
//			sigmoidLayer2.UpdateParameters(
//				new SigmoidLayer.Parameters{N = hiddent_layer, K = classes, IsOutputLayer = true}
//			);
//			classifierLayer.UpdateParameters(
//				new MultiClassBestSqrErrorLayer.Parameters{K = classes});
//			
//			sigmoidLayerDual1.UpdateParameters(
//				new SigmoidLayerDual.Parameters{N = input_size, K = hiddent_layer,dual_index = 1, Epsilon = epsilon}
//			);
//			sigmoidLayerDual2.UpdateParameters(
//				new SigmoidLayerDual.Parameters{N = hiddent_layer, K = classes,dual_index = 2,Epsilon = epsilon}
//			);			
//
//
//			Layer[] pipeline = new Layer[]{
//				inputLayer, 
//				sigmoidLayer1, 
//				sigmoidLayer2,
//				classifierLayer,
//				sigmoidLayerDual2,
//				sigmoidLayerDual1
//			};
//			
//
////			
////			
////			inputLayer.UpdateParameters(
////				new BatchingImagesOptimizedInputLayer.Parameters{
////					Width = input_width,
////					Height = input_height
////				
////				
////				}
////			);
////
////			sigmoidLayer2.UpdateParameters(
////				new SigmoidLayer.Parameters{N = input_size, K = classes, IsOutputLayer = true}
////			);
////			classifierLayer.UpdateParameters(
////				new MultiClassBestSqrErrorLayer.Parameters{K = classes});
////			
////
////			sigmoidLayerDual2.UpdateParameters(
////				new SigmoidLayerDual.Parameters{N = input_size, K = classes,dual = sigmoidLayer2,
////				Epsilon = epsilon}
////			);			
////
////
////			Layer[] pipeline = new Layer[]{
////				inputLayer, 
////				sigmoidLayer2,
////				classifierLayer,
////				sigmoidLayerDual2
////			};
////						
//			
//			
//			
//	
//			/* DFLOW magic */
//			Engine engine = new Engine ();
//			engine.dll_lookup_path = System.Environment.CurrentDirectory + "/{1}";
//			engine.plugin_lookup_path = System.Environment.CurrentDirectory + "/{1}";
//			engine.DoStartRunningEngine ();
//			engine.DataflowRemoveAll();
//			engine.DataflowNew ("df_main", true);			
//			
//			
//			Dataflow dflow = engine.GetCurrentDataflow();
//			
//			NeuralNetwork wionn = new NeuralNetwork();
//			
//
//			wionn.UpdateParameters( new NeuralNetwork.Parameters{
//				Architecture = pipeline,
//				TrainingDatasetSource = training_dataset_source,
//				TestDatasetSource = test_dataset_source,
//				BatchSize = batch_size
//			});	
//  
//			
//		
//			
//			dflow.AddNode(wionn);
//			
//			BsonDocument bd = dflow.Save ();
//	
//			Console.WriteLine(bd.ToString());
////			Dataflow dflow_test_render = new Dataflow();
////			Dictionary<string, Type> tmp = new Dictionary<string, Type>();
////			tmp[wionn.GetNodetype().ToString()] = wionn.GetType();
////			dflow_test_render.Load (bd, tmp);
//			
//			engine.OneStepIter();
//			
//			
//			inputLayer.source_dataset = test_dataset;		
//			foreach (object id in test_dataset.Keys()) {
//				dflow.data_address = id;
//				engine.OneStepIter ();
//
//				Globals.Logger.Log ("This is "+Convert.ToChar((int)((int)('0')+(int)classifierLayer.final_output.ptr)).ToString() );
//				
//				WIO.ComputingToolkit.Utils.DisplayOpenCLImage(new Bitmap((Image)test_dataset.GetElement(id)), waitkey:1000);
//
//			}
//			
//		}
//				
		
		public delegate Tuple<string,string,string> prepareDatasetDelegate(); 
		public static void RunMLP(int classes, prepareDatasetDelegate dataset_generator, 
		                          int batch_size, int hiddent_layer = 110, float epsilon = 0.01f, int input_width = 28, int input_height=28){
		
			int input_size = input_width * input_height;
			
			/*Prepare dataset and separate to training and test subsets*/
			string traning_dataset_source = dataset_generator().Item1;
			string testing_dataset_source = dataset_generator().Item2;
			string cache_source = dataset_generator().Item3;

//			/*Run everything*/
//			RunLogisticRegressionTrain(prep.Item1, prep.Item2);
			
			
			
//			
			/* Initialize layer architecture */
			BatchingImagesOptimizedInputLayer inputLayer = new BatchingImagesOptimizedInputLayer();
			SigmoidLayer sigmoidLayer1 = new SigmoidLayer();
			SigmoidLayer sigmoidLayer2 = new SigmoidLayer();
			MultiClassBestSqrErrorLayer classifierLayer = new MultiClassBestSqrErrorLayer();
			SigmoidLayerDual sigmoidLayerDual1 = new SigmoidLayerDual();
			SigmoidLayerDual sigmoidLayerDual2 = new SigmoidLayerDual();
			
			
			
			
			/*Set parameters (note no need to set next,prev. It will b set in parameters)*/
			inputLayer.UpdateParameters(
				new BatchingImagesOptimizedInputLayer.Parameters{
				Width = input_width,
				Height = input_height
				
			}
			);
			sigmoidLayer1.UpdateParameters(
				new SigmoidLayer.Parameters{N = input_size, K = hiddent_layer}
			);
			sigmoidLayer2.UpdateParameters(
				new SigmoidLayer.Parameters{N = hiddent_layer, K = classes, IsOutputLayer = true}
			);
			classifierLayer.UpdateParameters(
				new MultiClassBestSqrErrorLayer.Parameters{K = classes});
			
			sigmoidLayerDual1.UpdateParameters(
				new SigmoidLayerDual.Parameters{N = input_size, K = hiddent_layer,dual_index = 1, Epsilon = epsilon}
			);
			sigmoidLayerDual2.UpdateParameters(
				new SigmoidLayerDual.Parameters{N = hiddent_layer, K = classes,dual_index = 2,Epsilon = epsilon}
			);			


			Layer[] pipeline = new Layer[]{
				inputLayer, 
				sigmoidLayer1, 
				sigmoidLayer2,
				classifierLayer,
				sigmoidLayerDual2,
				sigmoidLayerDual1
			};
			

//			
//			
//			inputLayer.UpdateParameters(
//				new BatchingImagesOptimizedInputLayer.Parameters{
//					Width = input_width,
//					Height = input_height
//				
//				
//				}
//			);
//
//			sigmoidLayer2.UpdateParameters(
//				new SigmoidLayer.Parameters{N = input_size, K = classes, IsOutputLayer = true}
//			);
//			classifierLayer.UpdateParameters(
//				new MultiClassBestSqrErrorLayer.Parameters{K = classes});
//			
//
//			sigmoidLayerDual2.UpdateParameters(
//				new SigmoidLayerDual.Parameters{N = input_size, K = classes,dual = sigmoidLayer2,
//				Epsilon = epsilon}
//			);			
//
//
//			Layer[] pipeline = new Layer[]{
//				inputLayer, 
//				sigmoidLayer2,
//				classifierLayer,
//				sigmoidLayerDual2
//			};
//						
			
			
			
	
			/* DFLOW magic - TODO: remove that */
			Engine engine = new Engine ();
			engine.dll_lookup_path = System.Environment.CurrentDirectory + "/{1}";
			engine.plugin_lookup_path = System.Environment.CurrentDirectory + "/{1}";
			engine.DoStartRunningEngine ();
			engine.DataflowRemoveAll();
			engine.DataflowNew ("df_main", true);			
			
			
			Dataflow dflow = engine.GetCurrentDataflow();
			
			NeuralNetwork wionn = new NeuralNetwork();
			AggregatorSingleNode asn = new AggregatorSingleNode();
			WriteToDiskNode wmn = new WriteToDiskNode();


			
			

			wionn.UpdateParameters( new NeuralNetwork.Parameters{
				Architecture = pipeline,
				BatchSize = batch_size,
				BatchesToTrain = 100
			});	
  

			/* Wire output serialization */
			dflow.AddNode(asn);
			dflow.AddNode(wmn);
			dflow.AddWire(new Wire(wionn, "result",asn,"input"));
			dflow.AddWire(new Wire(asn, "output",wmn,"input"));
		
			
			
			//prepare the dataset, add node, wire it
			
			if(cache_source!="") {
				Console.WriteLine("Caching run");
				WIODatasetLanguageEnv.Instance.EvaluateDirectly(cache_source);
			}
			IDataset source_dataset = (IDataset)WIODatasetLanguageEnv.Instance.EvaluateDirectly(traning_dataset_source);
			IDataset test_dataset = (IDataset)WIODatasetLanguageEnv.Instance.EvaluateDirectly(testing_dataset_source);
			dflow.AddNode(wionn);
			dflow.source_dataset = source_dataset;

			int cnt = 0;
			if(cnt == 1) foreach(object id in source_dataset.Keys()){ WIO.ComputingToolkit.Utils.DisplayOpenCLImage(
				new System.Drawing.Bitmap((Image)source_dataset.GetElement(id)),"ShowImage",100); if(++cnt >50) break; }
						
			//just checking
			BsonDocument bd = dflow.Save ();
			Console.WriteLine(bd.ToString());

			
			//run until not trained
			while(dflow.State == Dataflow.DataflowState.RequiresTraining){
				foreach(object id in source_dataset.Keys ()){
					dflow.data_address = id;
					engine.OneStepIter();
				}	
			}
			
			if(dflow.State != Dataflow.DataflowState.Ready){
				throw new Exception("Dataflow not trained properly, sorry! Returned state is : "+dflow.State.ToString());
			}
			
			Globals.Logger.Log ("Training was successful. Every dataflow node is in Ready state");
			
			dflow.source_dataset = test_dataset;		
			foreach (object id in test_dataset.Keys()) {
				dflow.data_address = id;
				engine.OneStepIter ();

				Globals.Logger.Log ("This is "+Convert.ToChar
				                    ((int)((int)('0')+((List<int>)wionn.result)[0])
				                                                    ).ToString() );
				
				WIO.ComputingToolkit.Utils.DisplayOpenCLImage(new Bitmap((Image)test_dataset.GetElement(id)), waitkey:1000);

			}
			
		}		
		
		
		
		public static Tuple<object, object> PrepareMNISTDataset(){
			MNISTDataset ds = new MNISTDataset();
			ds.UpdateParameters(new MNISTDataset.Parameters{
				root_path = "./Data/MNIST/", cache = true});
			
			
			
			ShuffleDataset sd = new ShuffleDataset();
			sd.UpdateParameters( new ShuffleDataset.Parameters{dataset = ds, seed = 
				1});
				
				//(int)(WIO.ComputingToolkit.Utils.GetCurrentTimestamp()%1000)});
			
			TrainingDataset td = new TrainingDataset();
			td.UpdateParameters(new TrainingDataset.Parameters{removeItems = false, seed = 1,
				training_percentage = 0.75, source_dataset = sd});
			
			
			ExcludeDataset ed = new ExcludeDataset();
			ed.UpdateParameters(new ExcludeDataset.Parameters{dataset=sd, exdataset = td, removeItems = false});
			
			Globals.Logger.Log ("Created "+td.Count.ToString()+" training dataset and " + ed.Count.ToString() + " test dataset");
			
			
			return new Tuple<object,object>(td,ed);		
		}
		
		public static Tuple<object, object> PrepareMNISTDatasetSource(){
			MNISTDataset ds = new MNISTDataset();
			ds.UpdateParameters(new MNISTDataset.Parameters{
				root_path = "./Data/MNIST/", cache = true});
			string mnistdataset_cache_source = 
				"${\"MNISTDataset\":{\"root_path\":\"./Data/MNIST/\",\"cache\":true}}";
			string mnistdataset_source = 
				"{\"MNISTDataset\":{\"root_path\":\"./Data/MNIST/\",\"cache\":true}}";
			string shuffle_dataset = "{\"Shuffle\":{\"dataset\":"+mnistdataset_source+",seed:1}}";
			
			string training_dataset = "{\"TrainingDataset\":{\"removeItems\":false,\"seed\":1,\"training_percentage\":0.75," +
				"\"source_dataset\":"+shuffle_dataset+"}}";
			
			string testing_dataset = "{\"Exclude\":{\"dataset\":"+shuffle_dataset+",\"exdataset\":"+training_dataset+",\"removeItems\":false}}";
			
	

			return new Tuple<object,object>(mnistdataset_cache_source+"\n"+training_dataset,mnistdataset_cache_source+"\n"+testing_dataset);		
		}		
		
		
		public static Tuple<string,string,string> PrepareMNISTDatasetSourceWhole(){
			string mnistdataset_cache_source = 
				"${\"MNISTDataset\":{\"root_path\":\"./Data/MNIST/\",\"cache\":true}}";
			string mnistdataset_source = 
				"{\"MNISTDataset\":{\"root_path\":\"./Data/MNIST/\",\"cache\":true}}";
			string shuffle_dataset = "{\"Shuffle\":{\"dataset\":"+mnistdataset_source+",seed:1}}";
			
			string training_dataset = "{\"TrainingDataset\":{\"removeItems\":false,\"seed\":1,\"training_percentage\":0.75," +
				"\"source_dataset\":"+shuffle_dataset+"}}";
			
			string testing_dataset = "{\"Exclude\":{\"dataset\":"+shuffle_dataset+",\"exdataset\":"+training_dataset+",\"removeItems\":false}}";
			
	

			return new Tuple<string,string,string>(shuffle_dataset, testing_dataset,mnistdataset_cache_source);
			
		}				
		
		
			
		
		
		public static void TestPerformanceGPUSetElementArgs(){
		
			
			//first i create a test matrix
			float[,] wm1_dev = new float[500,500];
			WIOMatrixTests._FillRnd(wm1_dev);
			WIOOpenCLMatrix wm1 = new WIOOpenCLMatrix(wm1_dev);
			
			System.DateTime start;
				
			start = System.DateTime.Now;
			
			int ITER = 1000;
			for(int i=0;i<ITER;++i){
				wm1.SetElementGPU(3,1, wm1.GetElementGPU (3,1));
			}		
		
			
			TimeSpan ts = System.DateTime.Now - start;
			
			
			
			
			Console.WriteLine("Time elapsed = "  + ts.TotalMilliseconds );
			
			
			start = System.DateTime.Now;
			
			ITER = 1000;
			for(int i=0;i<ITER;++i){
				float tmp = wm1.GetElementGPUtest ();
				wm1.SetElementGPUtest(tmp);
			}		
		
			
			ts = System.DateTime.Now - start;
			
			
			Console.WriteLine("Time elapsed = "  + ts.TotalMilliseconds );			
			
			
		}		
		
//		public static void TestPerformanceGPUMApplyCloBinding(){
//			
//		
//
//			
//			float[,] wm1_dev = new float[500,500];
//				WIOMatrixTests._FillRnd(wm1_dev);
//						float[,] wm2_dev = new float[500,500];
//				WIOMatrixTests._FillRnd(wm2_dev);
////			WIOOpenCLMatrix wm1 = new WIOOpenCLMatrix(wm1_dev);
//			
//			
//			System.DateTime start;
//				
//			start = System.DateTime.Now;
//			
//			int ITER = 100000;
////			for(int i=0;i<ITER;++i){
////				wm1.MApplyLinear(1.0f,0.1f);
////			}		
//		
//			
//			TimeSpan ts = System.DateTime.Now - start;
//			
//			Console.WriteLine("Time elapsed = "  + ts.TotalMilliseconds );	
//			
//			
//			 ComputePlatform platform = ComputePlatform.Platforms[0];
//			Console.WriteLine(platform.Vendor);
//
//              // create context with all gpu devices
//              ComputeContext context = new ComputeContext(ComputeDeviceTypes.Gpu,
//               new ComputeContextPropertyList(platform), null, IntPtr.Zero);
//		
//			  ComputeCommandQueue commands = new ComputeCommandQueue(context, context.Devices[0], ComputeCommandQueueFlags.None);
//			
//			
//			
//			ClooMatrix wm2 = new ClooMatrix(wm2_dev, context, commands);
//			wm2.MApplyLinear(1.0f,0.001f);
//			start = System.DateTime.Now;
//		
//			for(int i=0;i<ITER;++i){
//				wm2.MApplyLinear(1.0f,0.001f);
//				
//			}		
//			
//			 ts = System.DateTime.Now - start;
//			Console.WriteLine("Time elapsed = "  + ts.TotalMilliseconds );	
//			
//			
//			
//			
//		}
		
		public static unsafe void NaiveMApply(float[,] M, float a, float b){
			for(int i=0;i<M.GetLength(0);++i) for(int j=0;j<M.GetLength(1);++j)
				M[i,j] = M[i,j]*a + b;
		}
		
		public static void TestPerformanceGPUMApply(){
			//first i create a test matrix
			float[,] wm1_dev = new float[100,28*28];
			WIOMatrixTests._FillRnd(wm1_dev);
			WIOOpenCLMatrix wm1 = new WIOOpenCLMatrix(wm1_dev);
			
			System.DateTime start;
		
			start = System.DateTime.Now;
			
			int ITER = 56000;
			for(int i=0;i<ITER;++i){
				NaiveMApply(wm1_dev,1.0f,0.1f);
				if(i%1000==0) Console.WriteLine(i);
			}		
		
			
			TimeSpan ts = System.DateTime.Now - start;
			
			
			
			float sum_time = 0.0f;
			Console.WriteLine("Time elapsed = "  + ts.TotalMilliseconds );
			
//			wm1.MApplyLinearTest(1.0f,0.1f);
			wm1.MApplyLinear(1.0f,0.1f);
			start = System.DateTime.Now;
			
//		
//			for(int i=0;i<ITER;++i){
//				sum_time+=wm1.MApplyLinearTest(1.0f,0.1f);
//			}	
			
			for(int i=0;i<ITER;++i){
				wm1.MApplyLinear(1.0f,0.1f);
			}	
			
			ts = System.DateTime.Now - start;
			
			
			Console.WriteLine("Time elapsed = "  + ts.TotalMilliseconds );			
//			Console.WriteLine("Time elapsed on gpu = "  + sum_time/1000.0f);	
		}
		
		
		
		/*
		 * 
		 * 			
			        string clProgramSource = @"
kernel void VectorAdd(
    global  read_only float* a,
    global  read_only float* b,
    global write_only float* c )
{
    int index = get_global_id(0);
    c[index] = a[index] + b[index];
}
";
			
            // pick first platform
            ComputePlatform platform = ComputePlatform.Platforms[0];

            // create context with all gpu devices
            ComputeContext context = new ComputeContext(ComputeDeviceTypes.Gpu,
                new ComputeContextPropertyList(platform), null, IntPtr.Zero);

      // Create the arrays and fill them with random data.
                int count = 10;
                float[] arrA = new float[count];
                float[] arrB = new float[count];
                float[] arrC = new float[count];

                Random rand = new Random();
                for (int i = 0; i < count; i++)
                {
                    arrA[i] = (float)(rand.NextDouble() * 100);
                    arrB[i] = (float)(rand.NextDouble() * 100);
                }

                // Create the input buffers and fill them with data from the arrays.
                // Access modifiers should match those in a kernel.
                // CopyHostPointer means the buffer should be filled with the data provided in the last argument.
                ComputeBuffer<float> a = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, arrA);
                ComputeBuffer<float> b = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, arrB);
                
                // The output buffer doesn't need any data from the host. Only its size is specified (arrC.Length).
                ComputeBuffer<float> c = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, arrC.Length);

                // Create and build the opencl program.
                ComputeProgram program = new ComputeProgram(context, clProgramSource);
                program.Build(null, null, null, IntPtr.Zero);

                // Create the kernel function and set its arguments.
                
			System.DateTime start = System.DateTime.Now;
			             ComputeCommandQueue commands = new ComputeCommandQueue(context, context.Devices[0], ComputeCommandQueueFlags.None);
		
			int ITER = 1000;
			for(int i=0;i<ITER;++i){
						ComputeKernel kernel = program.CreateKernel("VectorAdd");
				
		                kernel.SetMemoryArgument(0, a);
		                kernel.SetMemoryArgument(1, b);
		                kernel.SetMemoryArgument(2, c);
		
		                // Create the command queue. This is used to control kernel execution and manage read/write/copy operations.
		   
		                // Execute the kernel "count" times. After this call returns, "eventList" will contain an event associated with this command.
		                // If eventList == null or typeof(eventList) == ReadOnlyCollection<ComputeEventBase>, a new event will not be created.
		                commands.Execute(kernel, null, new long[] { count }, null, null);
		                
		                // Read back the results. If the command-queue has out-of-order execution enabled (default is off), ReadFromBuffer 
		                // will not execute until any previous events in eventList (in our case only eventList[0]) are marked as complete 
		                // by OpenCL. By default the command-queue will execute the commands in the same order as they are issued from the host.
		                // eventList will contain two events after this method returns.
//		                commands.ReadFromBuffer(c, ref arrC, true, null);
				
//				commands.Finish();

			}
			commands.ReadFromBuffer(c, ref arrC, true, null);
			Console.WriteLine((System.DateTime.Now - start).TotalMilliseconds);
			
                // A blocking "ReadFromBuffer" (if 3rd argument is true) will wait for itself and any previous commands
                // in the command queue or eventList to finish execution. Otherwise an explicit wait for all the opencl commands 
                // to finish has to be issued before "arrC" can be used. 
                // This explicit synchronization can be achieved in two ways:

                // 1) Wait for the events in the list to finish,
                //eventList.Wait();

                // 2) Or simply use
                
                // Print the results to a log/console.
                for (int i = 0; i < count; i++)
                    Console.WriteLine("{0} + {1} = {2}", arrA[i], arrB[i], arrC[i]);
		
		
		*/
		
		public static void TestBsonSerialization(){
			Tuple<IDataset, IDataset> tmp = PrepareGenDataset();
			
			IDataset test = tmp.Item1;
			
			Dictionary<string, float[]> tester = new Dictionary<string, float[]>();
			
			System.Random rnd = new System.Random();
			int cnt =0 ;
			foreach(object key in test.Keys()){
				tester[(cnt++).ToString()] = new float[]{(float)rnd.NextDouble(), (float)rnd.NextDouble()}; 
			}
			
			
			byte[] b = DFlowCore.BsonUtils.
				Encode(new BsonDocument("data",DFlowCore.BsonUtils.BsonEncode(tester, null)));
			FileStream f = System.IO.File.Open("./tmp.data", FileMode.Create);
				f.Write(b, 0, b.Length);
			f.Close();
			
			
			                             
	
			
			
			Dictionary<string, float[]> tester_load = null;
			
			long nbytes = (new System.IO.FileInfo("./tmp.data")).Length;
			
            b = new byte[nbytes];
            using (FileStream fs=new FileStream("./tmp.data", FileMode.Open)) {
				fs.Read(b, 0, (int)nbytes);
            	tester_load = (Dictionary<string, float[]>)
					DFlowCore.BsonUtils.BsonDecode(DFlowCore.BsonUtils.Decode(b)["data"], tester.GetType());
				fs.Close();
			}
				
		}
		
		public static void RunTests(){

			WIOComputingToolkitMain.RunTests();
		}
		
		
		public static void TestAggregate(){
		
			/* DFLOW magic - TODO: remove that */
			Engine engine = new Engine ();
			engine.dll_lookup_path = System.Environment.CurrentDirectory + "/{1}";
			engine.plugin_lookup_path = System.Environment.CurrentDirectory + "/{1}";
			engine.DoStartRunningEngine ();
			engine.DataflowRemoveAll();
			engine.DataflowNew ("df_main", true);			
			
			
			Dataflow dflow = engine.GetCurrentDataflow();
			
			WriteMongoNode wmn = new WriteMongoNode();
			dflow.AddNode(wmn);
			
			wmn.UpdateParameters(new WriteMongoNode.Parameters{
				TargetDatabase="aiosciweb1",
				TargetCollection="test_aggregate"
			});
			
			int create = 100;
		
			for(int i=0;i<create;++i){
				wmn.input = new List<Tuple<string, object>>();
				wmn.input.Add (new Tuple<string,object>("name","tmp"+i.ToString()));
				wmn.input.Add (new Tuple<string,object>("floats", new float[]{1.0f,2.0f}));
				
				engine.OneStepIter();
			}
			
			
		}
		
		public static void Main (string[] args)
		{
//			OpenCLNode.cmpq = Cl.CommandQueueProperties.ProfilingEnable;
			/*Test OPENCL and backend*/
//			
//			RunTests();

//			
			//TODO: refactor
			
		
			WIODatasetLanguageEnv.Instance.LoadAssembly(Assembly.GetCallingAssembly());			
			
//			Tuple<string, string,string> datasets = PrepareMNISTDatasetSourceWhole();
//			
//			string[] cmd = new string[]{"-d",
//				datasets.Item2,
//				"-m", "wionn_mlp_500hidden.bson",
//				"-t", datasets.Item1};
////			
//			DFlowCore.CommandTool cmdtool = new DFlowCore.CommandTool();
//			cmdtool.Load (cmd);
//			
			
			
//
//			TestAggregate();
			
			
//			
//			WIOOpenCLMatrix w = new WIOOpenCLMatrix(2,2);
//			
//			
				
//
//			Console.WriteLine(datasets.Item2);
//
			RunMLP(10, PrepareMNISTDatasetSourceWhole
			       ,100,500,0.2f,28,28);
/*Test results*/
/* -- 0.99 in 60 iterations : 1 batch size 150 hidden, full GPU 0.01f epsilon
*    0.93 in 12 iterations     : 100 batch size 500 hidden 0.2f epsilon, full GPU
*
*
*/
			
			
			
//			
//			IDataset test = (IDataset)env.Evaluate("${\"MNISTDataset\":{\"cache\":true,\"root_path\":\"./Data/MNIST\"}}");
//			Console.WriteLine(test.Count);
//			test = (IDataset)env.Evaluate(
//			  "{\"Shuffle\": {\"dataset\":{\"MNISTDataset\":{\"cache\":true,\"root_path\":\"./Data/MNIST\"}},\"seed\":1}}"
//			);
			Globals.Logger.Log ("Done...");
			while(true);
		}
		
		
		
	}
}




/*
 * 			        string clProgramSource = @"
kernel void VectorAdd(
    global  read_only float* a,
    global  read_only float* b,
    global write_only float* c )
{
    int index = get_global_id(0);
    c[index] = a[index] + b[index];
}
";
			
            // pick first platform
            ComputePlatform platform = ComputePlatform.Platforms[0];

            // create context with all gpu devices
            ComputeContext context = new ComputeContext(ComputeDeviceTypes.Gpu,
                new ComputeContextPropertyList(platform), null, IntPtr.Zero);

      // Create the arrays and fill them with random data.
                int count = 10;
                float[] arrA = new float[count];
                float[] arrB = new float[count];
                float[] arrC = new float[count];

                Random rand = new Random();
                for (int i = 0; i < count; i++)
                {
                    arrA[i] = (float)(rand.NextDouble() * 100);
                    arrB[i] = (float)(rand.NextDouble() * 100);
                }

                // Create the input buffers and fill them with data from the arrays.
                // Access modifiers should match those in a kernel.
                // CopyHostPointer means the buffer should be filled with the data provided in the last argument.
                ComputeBuffer<float> a = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, arrA);
                ComputeBuffer<float> b = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, arrB);
                
                // The output buffer doesn't need any data from the host. Only its size is specified (arrC.Length).
                ComputeBuffer<float> c = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, arrC.Length);

                // Create and build the opencl program.
                ComputeProgram program = new ComputeProgram(context, clProgramSource);
                program.Build(null, null, null, IntPtr.Zero);

                // Create the kernel function and set its arguments.
                
			System.DateTime start = System.DateTime.Now;
			             ComputeCommandQueue commands = new ComputeCommandQueue(context, context.Devices[0], ComputeCommandQueueFlags.None);
		
			int ITER = 1000;
			for(int i=0;i<ITER;++i){
						ComputeKernel kernel = program.CreateKernel("VectorAdd");
				
		                kernel.SetMemoryArgument(0, a);
		                kernel.SetMemoryArgument(1, b);
		                kernel.SetMemoryArgument(2, c);
		
		                // Create the command queue. This is used to control kernel execution and manage read/write/copy operations.
		   
		                // Execute the kernel "count" times. After this call returns, "eventList" will contain an event associated with this command.
		                // If eventList == null or typeof(eventList) == ReadOnlyCollection<ComputeEventBase>, a new event will not be created.
		                commands.Execute(kernel, null, new long[] { count }, null, null);
		                
		                // Read back the results. If the command-queue has out-of-order execution enabled (default is off), ReadFromBuffer 
		                // will not execute until any previous events in eventList (in our case only eventList[0]) are marked as complete 
		                // by OpenCL. By default the command-queue will execute the commands in the same order as they are issued from the host.
		                // eventList will contain two events after this method returns.
//		                commands.ReadFromBuffer(c, ref arrC, true, null);
				
//				commands.Finish();

			}
			commands.ReadFromBuffer(c, ref arrC, true, null);
			Console.WriteLine((System.DateTime.Now - start).TotalMilliseconds);
			
                // A blocking "ReadFromBuffer" (if 3rd argument is true) will wait for itself and any previous commands
                // in the command queue or eventList to finish execution. Otherwise an explicit wait for all the opencl commands 
                // to finish has to be issued before "arrC" can be used. 
                // This explicit synchronization can be achieved in two ways:

                // 1) Wait for the events in the list to finish,
                //eventList.Wait();

                // 2) Or simply use
                
                // Print the results to a log/console.
                for (int i = 0; i < count; i++)
                    Console.WriteLine("{0} + {1} = {2}", arrA[i], arrB[i], arrC[i]);
                    */