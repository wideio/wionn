using System;
using OpenCL;
using OpenCL.Net;


using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Drawing.Drawing2D;


using NUnit.Framework;

using System.IO;


using WIO.ComputingToolkit;
using WIO.Utils;


namespace WIO.NN.Obj
{
	#region Converters

	/// <summary>
	/// Converters. 
	/// 
	/// Main Idea is : 
	///		*if they pass* then data is correct and I can execute computations. 
	/// 	it makes code much cleaner and easier to maintain
	///
	/// </summary>
	
	public static unsafe class Converters{	
		
		
		static Converters(){
			WIO.Utils.ProfilerManager.RegisterTimer("ConvertBitmapTo1D");
			WIO.Utils.ProfilerManager.RegisterTimer("ConvertBitmapArrayToBatchMatrix");
			WIO.Utils.ProfilerManager.RegisterTimer("ConvertObjBitmapUnmanagedBatch1DToBatchMatrix");
			WIO.Utils.ProfilerManager.RegisterTimer("ConvertBitmapTo1DUnmanaged");
			WIO.Utils.ProfilerManager.RegisterTimer("Tmp");
		
		}
		
		public static double ConvertToBitmap1DProfiler = 0;
		public static double ConvertBitmapArrayToBatchMatrixProfiler = 0;
		
		
		[DFlow.SpecificConverter]
		public static unsafe ObjBatchWIOMatrix ConvertObjBitmapUnmanagedBatch1DToBatchMatrix(ObjBitmapUnmanagedBatch1D objin,
			
		                                  
		                                                                                     
		    ObjBatchWIOMatrix objout){
			
			
			
			WIO.Utils.ProfilerManager.StartProfilerTimer("ConvertObjBitmapUnmanagedBatch1DToBatchMatrix");
			if(objin.Ready == false){
				objout.Ready = false;
				return objout;
			}			
			ToolkitBackend used_backend = (ToolkitBackend)objout.parameters["toolkit_backend"];
			if(used_backend!=ToolkitBackend.OPENCL){
				throw new NotImplementedException("Not implemented backend in converter");
			}			
			if(objout.ptr == null) 
				objout.ptr = new WIOOpenCLMatrix(
					
					(int)objin.parameters["Width"]*(int)objin.parameters["Height"]
					
					,(int)objin.parameters["N"]);		
			
			
			Assert.AreEqual((string)objin.parameters["type"], "float"); //not supported if not
			Assert.AreEqual(
				(int)objin.parameters["Width"]*(int)objin.parameters["Height"], (objout.ptr as WIOOpenCLMatrix).GetLength(0));
			Assert.AreEqual(
				(int)objin.parameters["N"], (objout.ptr as WIOOpenCLMatrix).GetLength(1));
			
			
			WIOOpenCLMatrix target_matrix = (WIOOpenCLMatrix)objout.ptr;
			
			
			//I am not keeping FA in obj because I might want to change this in the future
			//but IntPtr is always good
			FixedArray<float> f_a = 
				new FixedArray<float>((IntPtr)objin.ptr, (int)objin.parameters["Width"]*(int)objin.parameters["Height"]*(int)objin.parameters["N"]);

			WIO.Utils.ProfilerManager.StartProfilerTimer("Tmp");
			target_matrix.RewireBuffer(f_a.arrayPtr,target_matrix.GetLength(0),target_matrix.GetLength(1),
			                           borrowing : false
			                           );
			WIO.Utils.ProfilerManager.StopProfilerTimer("Tmp");
			
			
//			WIOOpenCLMatrix display = new WIOOpenCLMatrix(
//				(int)objin.parameters[1],(int)objin.parameters[2]*(int)objin.parameters[3]);
//			display.RewireBuffer(f_a.arrayPtr,	(int)objin.parameters[1],
//			                     (int)objin.parameters[2]*(int)objin.parameters[3]);
//			WIO.ComputingToolkit.Utils.DisplayOWIOOpenCLMatrixAsImage(display);
//			
			
			objout.Ready = true;
				WIO.Utils.ProfilerManager.StopProfilerTimer("ConvertObjBitmapUnmanagedBatch1DToBatchMatrix");
			return objout;
			
		}
		
		
		/// <summary>
		/// Converts the bitmap to1 D unmanaged.
		/// 
		/// Note : for now it is a very *specific* converter. It takes 1 channel and
		/// converts it to float unmanaged array - so many assumptions.
		/// 
		/// Also it doesn't care about format of bitmap. For now it is working with standard .png,
		/// .jpg. But it something to fix to avoid bugs in future.
		/// </summary>
		[DFlow.SpecificConverter]
		public static unsafe ObjBitmapUnmanaged1D ConvertBitmapTo1DUnmanaged(Bitmap objin,
		                                                                     ObjBitmapUnmanaged1D objout){
			
			//it is very easy to mess up something with conversions etc : that's why I am doing so many
			//asserts
			WIO.Utils.ProfilerManager.StartProfilerTimer("ConvertBitmapTo1DUnmanaged");
			Assert.AreEqual((int)objout.parameters["Width"]*(int)objout.parameters["Height"], objin.Width*objin.Height);
			Assert.AreEqual((string)objout.parameters["type"], "float"); //not supported if not
			if(objout.ptr == null){
				WIO.Utils.FixedArray<float> target = new WIO.Utils.FixedArray<float>(objin.Width*objin.Height);
				objout.ptr = target.arrayPtr;
			}
			byte bitsPerPixel = 4*8;
			BitmapData bData = objin.LockBits(
				new Rectangle(0, 0, objin.Width, objin.Height), ImageLockMode.ReadWrite, 
				objin.PixelFormat);
			Assert.AreEqual(objin.PixelFormat,PixelFormat.Format32bppArgb);
			//CANNOT copy directly because of channels
			byte* scan0 = (byte*)bData.Scan0.ToPointer();
			float * scan0target = ((float*)(IntPtr)objout.ptr);
			for(int i=0;i<objin.Width;++i)
				for(int j=0;j<objin.Height;++j){
					byte* data = scan0 + (j) * bData.Stride + (i) * bitsPerPixel / 8;
					float* target = (float*)(scan0target + i*objin.Height + j);
					float tmp = (  (float)((float)data[1]+(float)data[2]+(float)data[0])/3.0f - 127.0f)/127.0f; 
				float tmp1 = (float)data[0];
				float tmp2 = (float)data[1];
				float tmp3= (float)data[2];
				*target = tmp;
				}	
			
			objin.UnlockBits(bData);
			
			objout.Ready = true;
			WIO.Utils.ProfilerManager.StopProfilerTimer("ConvertBitmapTo1DUnmanaged");
			return objout;
			
		}
		
		
		[DFlow.SpecificConverter]
		public static Obj1DWIOVect ConvertBitmapTo1D(ObjBitmap objin,
		                                                  Obj1DWIOVect objout){
			WIO.Utils.ProfilerManager.StartProfilerTimer("ConvertBitmapTo1D");
			//Wait this object to be ready
			if(objin.Ready == false){
				objout.Ready = false;
				return objout;
			}			
			
			System.DateTime start = System.DateTime.Now;
			
			
			
			ToolkitBackend used_backend = (ToolkitBackend)objout.parameters["toolkit_backend"];

			if(used_backend!=ToolkitBackend.OPENCL){
				throw new NotFiniteNumberException("Not implemented backend in converter");
			}
			
			WIOOpenCLMatrix target_vect = (WIOOpenCLMatrix)objout.ptr;
			

			System.Drawing.Image source_img = (System.Drawing.Image)objin.ptr;
			
			System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(source_img);
			
			float[,] input_bitmap = WIO.ComputingToolkit.Utils.Float2DMatrixFromBitmap(
				bmp);
			
//			float[,] input_bitmap = new float[source_img.Width, source_img.Height];
//			
	
			bmp.Dispose();
			
			
			Assert.AreEqual(target_vect.GetLength(0), input_bitmap.GetLength(0)*input_bitmap.GetLength(1));
			
			float[,] input = 
				new float[input_bitmap.GetLength(0)*input_bitmap.GetLength(1),1];
			
			int idx = 0;			
			int count = 0;
			float sum = 0.0f;
			foreach(float x in input_bitmap) {sum+=x; count++;}	
			//0 mean
			foreach(float x in input_bitmap) input[idx++,0] = (x - (sum/count)) / 255.0f; //enumerator..				
		
			target_vect.RewriteBuffer(input);
			

			
			WIO.Utils.ProfilerManager.StopProfilerTimer("ConvertBitmapTo1D");
			return new Obj1DWIOVect(used_backend,target_vect);
		}
		
		
		[DFlow.SpecificConverter]
		public static ObjBatchWIOMatrix ConvertBitmapArrayToBatchMatrix(ObjBitmapArray objin,
		                                                  ObjBatchWIOMatrix objout){
			
			WIO.Utils.ProfilerManager.StartProfilerTimer("ConvertBitmapArrayToBatchMatrix");
			//Wait this object to be ready
			if(objin.Ready == false){
				objout.Ready = false;
				return objout;
			}
			
			System.DateTime start = System.DateTime.Now;
			
			ToolkitBackend used_backend = (ToolkitBackend)objout.parameters["toolkit_backend"];

			if(used_backend!=ToolkitBackend.OPENCL){
				throw new NotImplementedException("Not implemented backend in converter");
			}
			
			List<System.Drawing.Bitmap> bmpList = objin.ptr as List<System.Drawing.Bitmap>;
			int input_size = bmpList.First ().Width*bmpList.First ().Height;
			int current_index = 0;
			
			//Create buffer, managed unfortunately
			float [] buffer = new float[input_size*bmpList.Count];
			float [] buffer_small = new float[input_size];
			foreach(System.Drawing.Bitmap bmp in bmpList){	
				WIO.ComputingToolkit.Utils.Float1DFromBitmap(bmp,
					buffer_small);
				
				
				//make it zero mean
				int count = 0;
				int idx = 0;
				float sum = 0.0f;
				foreach(float x in buffer_small) {sum+=x; count++;}	
				foreach(float x in buffer_small) buffer_small[idx++] = (x - 124.0f) / 255.0f; //enumerator..			
				
				
				buffer_small.CopyTo(buffer, current_index*input_size);
				
				++current_index;
			}
			

			if(objout.ptr == null) 
				objout.ptr = new WIOOpenCLMatrix(input_size,bmpList.Count);
			
			
			WIOOpenCLMatrix objout_mtr = (WIOOpenCLMatrix)objout.ptr;
			objout_mtr.RewriteBuffer(buffer,input_size,bmpList.Count);
//			try{
//				for(int i=1;i<=objout_mtr.GetLength(0);++i){
//					for(int j=1;j<=objout_mtr.GetLength(1);++j){
//						
//						objout_mtr[i,j] = buffer[(j-1)*input_size+i-1];
//						
//					}
//				}
//			}
//			catch(Exception e){
//			
//			
//				Console.WriteLine(e.ToString());
//			}
			
			//(objout.ptr as WIOOpenCLMatrix).RewireBuffer(buffer, bmpList.First ().Width*bmpList.First ().Height, bmpList.Count);
			
			objout.Ready = true;
			WIO.Utils.ProfilerManager.StopProfilerTimer("ConvertBitmapArrayToBatchMatrix");
			
		
			return objout;
		}
	}
	#endregion Converters
}

