using System;

using DFlow;
using DFlowCore;

using WIO.ComputingToolkit;
using WIO.Utils;
using WIODataLanguage;
using WIODatasetLanguage;

using System.Linq;
using System.Drawing;
using System.Reflection;

using WIO.NN.Obj;

using NUnit.Framework;

namespace WIO.NN
{

	public class SoftmaxLayerDual : DualLayer{
		
		public static double SoftmaxLayerDualProfiler = 0;
		
		[DFlow.InputPin()]
		///this is \delta^{l} (notation from UFDL tutorial)
		public WIO.NN.Obj.ObjBatchWIOMatrix input;	 
		
		private int _counterGradient = -1;
		
		//gpu computing buffers
		public WIOOpenCLMatrix gradient_buffer;
		public WIOOpenCLMatrix gradient_buffer_acc;
		public SoftmaxLayerDual(){

		}
		
		public override void Process(){
			System.DateTime start = System.DateTime.Now;
			
			SoftmaxLayer dual = this.Dual as SoftmaxLayer;
			SoftmaxLayer.Parameters p = (SoftmaxLayer.Parameters)dual.GetParameters();	
			
			if((ToolkitBackend)input.parameters["toolkit_backend"] != ToolkitBackend.OPENCL) 
				throw new NotImplementedException("Not implemented backend");
			if((ToolkitBackend)dual.output.parameters["toolkit_backend"] != ToolkitBackend.OPENCL) 
				throw new NotImplementedException("Not implemented backend");			
//			
			
				
			//some needed stuff
			WIOOpenCLMatrix delta_l = (WIOOpenCLMatrix)input.ptr;
			WIOOpenCLMatrix dual_activation = (WIOOpenCLMatrix)dual.input.ptr;			
//
			for(int i=1;i<=delta_l.GetLength(0);++i)
				delta_l[i,1]=delta_l[i,1]*(GetParameters() as Parameters).Epsilon;
			//matrixop
//			dual_activation.Update();
			dual_activation.Transpose();
			delta_l.Multiply(dual_activation,_self : false, dest : this.gradient_buffer);
			dual_activation.Transpose();	
			_counterGradient ++;
			this.gradient_buffer_acc.Add (this.gradient_buffer,_self : true);
			

			//accumualte and zero accumulator
			if(_counterGradient == (GetParameters() as Parameters).GradientComposeAverage){
				this.gradient_buffer_acc.MApplyLinear(
					1.0f/
					(GetParameters() as Parameters).GradientComposeAverage,0.0f);
				
				
				dual.W.Add(this.gradient_buffer_acc, _self: true);
				dual.b.Add(delta_l);	
				
				this.gradient_buffer_acc.MApplyLinear(0.0f,0.0f);
				_counterGradient = 0;
			}	

			
			//			//return it to original state, for now transpose is self-operator
			
////			
//			
			
//			this.gradient_buffer.Update();
//			
////			
//			for(int row =0; row < p.K;++row){
//				for(int col =0; col < p.N;++col){
////					dual.W[row+1,col+1]+= (((int) dual.meta.ptr == row ? 1 : 0) - 
////					                        ((WIOOpenCLMatrix)(dual.output.ptr))[row+1,1])*((WIOOpenCLMatrix)dual.input.ptr)[col+1,1];
////			
//					float tmp3 = (((int) dual.meta.ptr == row ? 1 : 0) - 
//					              ((WIOOpenCLMatrix)(dual.output.ptr))[row+1,1]);
//					float tmp4 = delta_l[row+1,1];
//					
//					Assert.IsTrue (tmp3 == tmp4);
//					
//					float delta3 = delta_l[row+1,1]*((WIOOpenCLMatrix)dual.input.ptr)[col+1,1];
//					
//					float delta_buffer = this.gradient_buffer[row+1,col+1];
//					
//					
//					Console.WriteLine (delta3);
//					
//					dual.W[row+1,col+1]+= delta_l[row+1,1]
//					                       *((WIOOpenCLMatrix)dual.input.ptr)[col+1,1];
//				}
//				dual.b[row+1,1]=(((int) dual.meta.ptr == row ? 1 : 0) - ((WIOOpenCLMatrix)(dual.output.ptr))[row+1,1]);
//			}
			SoftmaxLayerDualProfiler += (System.DateTime.Now - start).TotalMilliseconds;
		}
		
		public override void UpdateParameters (object o)
		{
			Parameters casted_parameters = (Parameters)o;
				
			this.parameters = casted_parameters;	
				
			SoftmaxLayer dual = this.Dual as SoftmaxLayer;
			SoftmaxLayer.Parameters p = (SoftmaxLayer.Parameters)dual.GetParameters();	
			
//			//prepare buffer
			this.gradient_buffer = new WIOOpenCLMatrix(dual.W.GetLength(0), dual.W.GetLength(1));
			this.gradient_buffer_acc = new WIOOpenCLMatrix(dual.W.GetLength(0), dual.W.GetLength(1));
			this.gradient_buffer_acc.MApplyLinear(0.0f,0.0f);
		}
		
		public new class Parameters : DualLayer.Parameters{
			public int GradientComposeAverage =2000 ; //how often should this layer change weights?
			public float Epsilon = 0.11f;
		}
	}
	
	public class SoftmaxLayer : Layer{
		public static double SoftmaxLayerProfiler = 0;
		
		
		#region PINS
		
		
		
		[DFlow.InputPin()]
		public Obj1DWIOVect input;
		[DFlow.OutputPin()]
		public Obj1DWIOVect output;

		#endregion PINS
		
	
		
		
		public WIOMatrix W,b;
		public WIOMatrix current_input;
	
		
		public new class Parameters : Layer.Parameters{
			/// <summary>
			/// Dimensionality of the input
			/// </summary>
			public int N; 
			/// <summary>
			/// Number of classes
			/// </summary>
			public int K; 
		
			/// <summary>
			/// The w_rnd_min controls range in which random activation takes place.
			/// There are some theoretical results about appropriate ranges
			/// But who cares for now
			/// </summary>
			public float w_rnd_min = -0.0001f, w_rnd_max = 0.0001f;
		}
		
		public void UpdateParameters(object p){
			Parameters casted_parameters = (Parameters)p;
			
			Globals.Logger.Log("Updating parameters in SigmoidLayer");
			
			this.parameters = new Parameters{ 
				N = casted_parameters.N, 
				K = casted_parameters.K, 
				backend = casted_parameters.backend
			};	
			
			if(casted_parameters.backend != ToolkitBackend.OPENCL) throw new NotImplementedException("Not implemented backend");
			
			
			
			
			this.input = new Obj1DWIOVect(casted_parameters.backend);
			this.input.ptr = new WIOOpenCLMatrix(casted_parameters.N,1);
			
			float[,] rndInitW = new float[casted_parameters.K, casted_parameters.N];
			float[,] rndInitB = new float[casted_parameters.K, 1];
			System.Random rnd = new System.Random((int)(WIO.ComputingToolkit.Utils.GetCurrentTimestamp()%100));
			
			
			
			
			for(int i=0;i<casted_parameters.K;++i){
				rndInitB[i,0]=casted_parameters.w_rnd_min + 
						(casted_parameters.w_rnd_max - casted_parameters.w_rnd_min)*(float)rnd.NextDouble();
				for(int j=0;j<casted_parameters.N;++j)
					rndInitW[i,j]=casted_parameters.w_rnd_min + 
						(casted_parameters.w_rnd_max - casted_parameters.w_rnd_min)*(float)rnd.NextDouble();
			}
			
			this.W = new WIOOpenCLMatrix(rndInitW);
			this.b = new WIOOpenCLMatrix(rndInitB);
//			
			this.output = new Obj1DWIOVect(WIO.NN.ToolkitBackend.OPENCL,
			                                   new WIOOpenCLMatrix(casted_parameters.K, 1));				
			
		}
		


		public override void Process ()
		{	
			try{
	
				if((ToolkitBackend)this.input.parameters["toolkit_backend"] != ToolkitBackend.OPENCL) throw new
					NotImplementedException("Not implemented backend");
				if((ToolkitBackend)this.output.parameters["toolkit_backend"] != ToolkitBackend.OPENCL) throw new
					NotImplementedException("Not implemented backend");
				
				
				System.DateTime start = System.DateTime.Now;
				
                WIOOpenCLMatrix input_matrix = (WIOOpenCLMatrix)this.input.ptr;
				WIOOpenCLMatrix output_matrix = (WIOOpenCLMatrix)this.output.ptr;
				Assert.AreEqual(input_matrix.GetLength(0), ((Parameters)this.parameters).N);
//				//it is ugly but buffered on GPU, no way to predict from W1*W2 wheter to 
//				//create new buffer or not in my opinion
//				
//				
//				
				this.W.Multiply(input_matrix, _self:false, dest: output_matrix); //W*X
				output_matrix.Add (this.b, _self:true); //W*X  + B
				output_matrix.MApplyExp(); //exp(WX + B)
				float summed_out = 0.0f;//output_matrix.ReduceSum(); 
				for(int i=0;i<output_matrix.GetLength(0);++i){
					summed_out+=output_matrix[i+1,1];
				}
				output_matrix.MApplyLinear(1.0f/summed_out, 0.0f); //   norm(exp(WX+B))	
//				((WIOOpenCLMatrix)output_matrix).Update();
//				float summed_out2 = output_matrix.ReduceSum(); //sanity check if it is 1.0f
			
			
				SoftmaxLayerProfiler += (System.DateTime.Now - start).TotalMilliseconds;
			}
			catch(Exception e){
				Globals.Logger.LogError(e.ToString());
				throw e;
			}
		}
		
		
	
	}
	

	
	
	
}

