using System;


using WIO.ComputingToolkit;
using WIO.Utils;
using WIODataLanguage;
using WIODatasetLanguage;

using DFlow;
using DFlowCore;

using System.IO;
using System.Linq;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using MongoDB.Bson;

using NUnit.Framework;
namespace WIO.NN
{
	
	/// <summary>
	/// Neural network main node, it can be easily extensible and hold many network architectures.
	/// 
	/// For now it assumes neural network that performs classification with test and training dataset
	/// Probably should inherit after more general
	/// 
	/// *Note*: it doesn't work unless it is first in the dataflow. It is thing to fix,
	/// it is because it passes source_dataset to inner dataflow. 
	/// </summary>
	public class NeuralNetwork : DFlow.TrainableNode{


		
		private void _ensure_right_dim_for_batch_pin(Layer l, FieldInfo pin, bool create_buffer = false){
			WIO.NN.Obj.WIONNObj obj = (WIO.NN.Obj.WIONNObj)pin.GetValue(l);
			
			if((ToolkitBackend)obj.parameters["toolkit_backend"]!=ToolkitBackend.OPENCL) throw new NotImplementedException();
			
			
			if(obj.ptr == null || (obj.ptr as WIOOpenCLMatrix).GetLength(1) != (int)this._dflow.GetGlobalVariable("batch_size"))
			{
				int rows = (int)obj.parameters["rows"];
				if(obj.ptr != null) (obj.ptr as WIOOpenCLMatrix).Dispose();
				
				
				pin.SetValue(l, 
				new WIO.NN.Obj.ObjBatchWIOMatrix( WIO.NN.ToolkitBackend.OPENCL,rows, create_buffer==true?
				                                (object)(new
	
				              WIOOpenCLMatrix(rows, Math.Max(1,(int)this._dflow.GetGlobalVariable("batch_size")), l is InputLayer
				                )):null
				                                )
				             );                                                                            					                                                                                           
			}
		}
		/// <summary>
		/// _precheck_dlow goes layer by layer in it's inner dflow and checks if
		/// </summary>
		private void _precheck_dlow(){
		
			//iterate over each layer and check if batch sizes buffer are correct for bufferable, if not replace them
			//note: it is a prototype, it should be though over more carefully how to do it
			//probably through some DFlow event managment : no time to look into it
			//becuase it is dflow's responsibility to ensure that input and output pins are correctly parametrized
			foreach(Layer l in (GetParameters() as Parameters).Architecture){
				
				//_ensure_right_dim_for_batch pin invokes
				//TODO: check if needed buffer (should be optional)
				FieldInfo input_pin = l.ListInputPins().First();
				WIO.NN.Obj.WIONNObj input_object = (WIO.NN.Obj.WIONNObj)input_pin.GetValue(l);
				if(input_object is WIO.NN.Obj.ObjBatchWIOMatrix || input_object == null)
					this._ensure_right_dim_for_batch_pin(l, input_pin);
				
				FieldInfo output_pin = l.ListOutputPins().First();
				WIO.NN.Obj.WIONNObj output_object = (WIO.NN.Obj.WIONNObj)output_pin.GetValue(l);
				if(output_object is WIO.NN.Obj.ObjBatchWIOMatrix || output_object == null)
					this._ensure_right_dim_for_batch_pin(l, output_pin, true);				 
				
				input_object = (WIO.NN.Obj.WIONNObj)input_pin.GetValue(l);
				output_object = (WIO.NN.Obj.WIONNObj)output_pin.GetValue(l);
				//make sure invariants are kept.
				input_object.Ready = false;
				output_object.Ready = false;
			}
			
			
		
		}

		private void _init ()
		{
			if(this.Initialized) return;
			
			
			
			
			
			
			/* DFLOW magic */
			this._engine = new Engine ();
			this._engine.dll_lookup_path = System.Environment.CurrentDirectory + "/{1}";
			this._engine.plugin_lookup_path = System.Environment.CurrentDirectory + "/{1}";
			this._engine.DoStartRunningEngine ();
			this._engine.DataflowRemoveAll();
			this._engine.DataflowNew ("df_main", true);			
			
			
			this._dflow = this._engine.GetCurrentDataflow();
			
			Globals.Logger.Log ("Engine and DFlow initialized");
			
			NeuralNetwork.Parameters casted_parameters = (NeuralNetwork.Parameters)GetParameters();
			
			Globals.Logger.Log ("Parameters fetched");
			
			//It is hell of imporant not to SET in parameters anything, because it can really
			//mess the serialization (for instance I had this bug with setting dual in DualLayer so it serialized it)
			
	
			
			Globals.Logger.Log ("Datasets created");

			foreach (Layer l in casted_parameters.Architecture) 
				this._dflow.AddNode (l);
	

			
			for(int i=0;i<casted_parameters.Architecture.Length;++i){
				Layer l = casted_parameters.Architecture[i];
				Layer.Parameters casted_local_params = (Layer.Parameters)l.GetParameters();
				
				//all layers follow input->output and meta->meta wires.
				//meta get passed along the network, just because might be useful
				if(i!=casted_parameters.Architecture.Length-1){
					this._dflow.AddWire(new Wire(l, "output",casted_parameters.Architecture[i+1],"input"));
					this._dflow.AddWire(new Wire(l, "meta",casted_parameters.Architecture[i+1],"meta"));
				}
			}

			
			
			if(casted_parameters.InitializationProcedure != null)
			foreach(var l in casted_parameters.InitializationProcedure)
				l.Init();
			
			if(FinalLayer == null){
				
				for(int i=0;i<casted_parameters.Architecture.Length;++i){
					if(casted_parameters.Architecture[i] is DualLayer) {
						FinalLayer = (MultiClassBestSqrErrorLayer)casted_parameters.Architecture[i-1];
						break;
					}
				}
				
			}
			for(int i=0;i<casted_parameters.Architecture.Length;++i){
				if(casted_parameters.Architecture[i] is DualLayer) {
					DualLayer.Parameters p = casted_parameters.Architecture[i].GetParameters() as DualLayer.Parameters;
					if(((DualLayer)casted_parameters.Architecture[i]).Dual == null){
						if(p.dual_index == -1) throw new Exception("Wrong architecture - no dual set in DualLayer");
						((DualLayer)casted_parameters.Architecture[i]).Dual = casted_parameters.Architecture[p.dual_index];
					}
				}
			}			
			//Make sure flow is enabled. Design choice is that next layer after final layer
			//is the first layer of backprop data flow
			FinalLayer classification_layer = FinalLayer;
			for(int i = 0 ;i<casted_parameters.Architecture.Length-1;++i){
				if(casted_parameters.Architecture[i] == classification_layer &&
					   i != casted_parameters.Architecture.Length-1)
						((DualLayer)(casted_parameters.Architecture[i+1])).SetFlow(true);
			}
				
			
			this.Initialized = true;
			this.CurrentBatch = 0;
			this.CurrentIteration = 0;
			
			this._predicted_correctly = this._predicted_incorrectly = this._idx = this._size_calculated = 0;
			this.State = NodeState.RequiresTraining;
			
			
			
			//set global variable - will be useful for nodes in the way
			this._dflow.SetGlobalVariable("batch_size", casted_parameters.BatchSize);
//			this._dflow.SetGlobalVariable("batch_size", 20);
		}
		
		public override void UpdateParameters (object p)
		{
			base.UpdateParameters(p);
			
			this.parameters = p; //TODO: add clone here..
			this._init();
			Parameters casted_parameters = this.parameters as Parameters;
			

			string descriptor = "";
			foreach(Layer l in casted_parameters.Architecture) descriptor+=l.GetDescriptor();
			this.Descriptor = descriptor;

			
		}
		
		
		/// <summary>
		/// Runnings the process in normal, feedforward mode.
		/// </summary>
		public override void RunningProcess ()
		{
			
			
			Assert.IsTrue (this.Initialized);		
			
			Parameters casted_parameters = (Parameters)GetParameters();
			InputLayer inputLayer = (InputLayer)casted_parameters.Architecture[0];

			///Run test after each batch
			this._dflow.source_dataset = this.host_Dataflow.source_dataset;
			this._dflow.data_address = this.host_Dataflow.data_address; //rewire
			this._dflow.SetGlobalVariable("batch_size",1);
			int batch_size = (int) this._dflow.GetGlobalVariable("batch_size");
			try{
				FinalLayer classification_layer = FinalLayer;
				classification_layer.ClassificationPhase = true;
	
				this._engine.OneStepIter ();
				
				this.result = FinalLayer.final_output.ptr; //just rewiring
			}
			catch(Exception e){
				Globals.Logger.LogError("Error in inner dataflow "+e.ToString());
				throw e;
			}
		}


        public override  void LoadModel(Type t = null)
        {
			Dictionary<string,object> data_to_load = new Dictionary<string, object>();
			
            Parameters p = GetParameters() as Parameters;
            long nbytes = (new System.IO.FileInfo(p.storageurl)).Length;
            byte[] b = new byte[nbytes];
            using (FileStream fs=new FileStream(p.storageurl, FileMode.Open)) {
				fs.Read(b, 0, (int)nbytes);
            	data_to_load = (Dictionary<string, object>)DFlowCore.BsonUtils.BsonDecode(DFlowCore.BsonUtils.Decode(b)["data"], 
				                                              data_to_load.GetType());
			}
			
			if(data_to_load["descriptor"] != (string)this.Descriptor){
				throw new Exception("Wrong persistent data format");
			}
			
			List<Tuple<string, object>> data_to_load2 = 
			   (List<Tuple<string,object>>)data_to_load["architecture_data"];
			for(int layer = 0; layer < data_to_load.Count; ++ layer){
				Assert.AreEqual(data_to_load2[layer].Item1, p.Architecture[layer].GetDescriptor()); //always be on the safe side, however it should be fine..
				p.Architecture[layer].LoadPersistentData(data_to_load2[layer].Item2);
			}
			
			Globals.Logger.Log ("Loaded model successfully");
			
        }	
		
		
		
		
		public override void SaveModel ()
		{
			Dictionary<string, object> data_to_save = new Dictionary<string, object>();
			
			Parameters casted_parameters = this.parameters as Parameters;
			List<Tuple<string, object>> data_to_save2 = new List<Tuple<string, object>>();
			//idea is to check descriptors on the go (so double check for security)
			
			
			
			foreach(Layer l in casted_parameters.Architecture)
				data_to_save2.Add (new Tuple<string, object>(l.GetDescriptor(), l.GetPersistentData()));
			
			data_to_save["architecture_data"] = data_to_save2;
			data_to_save["descriptor"] = this.Descriptor;
			
            byte[] b = DFlowCore.BsonUtils.
				Encode(new BsonDocument("data",DFlowCore.BsonUtils.BsonEncode(data_to_save, null)), maximum_size :160000000);
			File.Open(casted_parameters.storageurl, FileMode.Create).Write(b, 0, b.Length);	
			
			Globals.Logger.Log ("Saved model successfully descriptor "+this.Descriptor);
		}
		
		/// <summary>
		/// Trainings the node, so dflow won't go any futher until it is not trained.
		/// 
		/// 
		/// The idea is that if DFlow tries to send an object to node that is not trained, it will train itself
		/// not using this object and then allow for dflow futher
		/// 
		/// 
		/// </summary>
		public override void TrainingProcess ()
		{
			Assert.IsTrue (this.Initialized);
			
			Parameters casted_parameters = (Parameters)GetParameters ();
			InputLayer inputLayer = (InputLayer)casted_parameters.Architecture [0];

			
			object current_id = this.host_Dataflow.data_address;
			//count if changed
			if(this._currentDataset != this.host_Dataflow.source_dataset){
				this._currentDatasetSize =  this.host_Dataflow.source_dataset.Count;
				this._currentDataset = this.host_Dataflow.source_dataset;
			}

		
			if(this.CurrentIteration == 0){
				//preparation for processing training
				Globals.Logger.Log ("Preparations for training iteration "+this.CurrentIteration);
		
				foreach (Layer l in casted_parameters.Architecture) {
					l.Reset ();
				}		
				
				this._dflow.SetGlobalVariable("batch_size", (GetParameters() as Parameters).BatchSize);
				this._precheck_dlow();
				//FIXME: it is wrong in general
				//if we have many nodes, need to be fixed
				this._dflow.source_dataset = this.host_Dataflow.source_dataset; 

				FinalLayer.ClassificationPhase = false;				
			}
		

			//We are training	
			if(this.CurrentIteration<= (int)this._currentDatasetSize*casted_parameters.TrainPercentage){
				this._dflow.data_address = current_id; 
				this._engine.OneStepIter ();	//get rid of this
				if (this.CurrentIteration % casted_parameters.ReportFrequency == 0 && this.CurrentIteration != 0 ) {
					foreach(var x in WIO.Utils.ProfilerManager.DumpMarkedTimes()){
						if(x.Item2==0.0f) continue;
						Console.WriteLine(x.ToString());
						WIO.Utils.ProfilerManager.SetMark(x.Item1);
					}
					
				}
			}
			//We are testing
			else{

				if(this.CurrentIteration==(int)this._currentDatasetSize*casted_parameters.TrainPercentage+1){
					Globals.Logger.Log ("Preparations for testing iteration "+this.CurrentIteration);
					foreach (Layer l in casted_parameters.Architecture) {
						l.Reset ();
					}
				
					this._predicted_correctly = 0;
					this._predicted_incorrectly = 0;
					this._precheck_dlow();	
					FinalLayer.ClassificationPhase = true;
					this._dflow.source_dataset = this.host_Dataflow.source_dataset; //relinking				
					Globals.Logger.Log("Batch "+this.CurrentBatch.ToString()+" testing..");	
					
					this._idx = 0;
					this._size_calculated =0;
					
					this._ids = new List<object>();
					this._confusion_matrix = new int[10,10];
					for(int m=0;m<10;++m) for(int j=0;j<10;++j) this._confusion_matrix[m,j]=0;				
				}
				
		
		
				this._dflow.data_address = current_id;
				this._ids.Add (current_id);
				this._engine.OneStepIter ();
				if((this._idx+1)%(Math.Max (1,(int)this._dflow.GetGlobalVariable("batch_size") ))== 0){
					
					for(int w=0;w<(Math.Max (1,(int)this._dflow.GetGlobalVariable("batch_size")));++w){
						//TODO: generalize
						if((FinalLayer.meta.ptr as List<int>)[w] == 
						 (FinalLayer.final_output.ptr as List<int>)[w]) 
						{
							this._predicted_correctly ++;	
						}
						else{
							
							if(this._predicted_incorrectly%4000 == 0){
//									WIO.ComputingToolkit.Utils.DisplayOpenCLImage(
//										new Bitmap((Image)TestDataset.GetElement(ids[w])), waitkey:1000
//									);
								Globals.Logger.Log ((FinalLayer.final_output.ptr as List<int>)[w].ToString()
								                    +"<="+(FinalLayer.meta.ptr as List<int>)[w].ToString()
								                    );
							}
							
							this._confusion_matrix[(FinalLayer.final_output.ptr as List<int>)[w],
							                 (FinalLayer.meta.ptr as List<int>)[w]]+=1;
							this._predicted_incorrectly++;
						}
					}
					this._ids.Clear();
					this._size_calculated += Math.Max (1,(int)this._dflow.GetGlobalVariable("batch_size"));
					 (FinalLayer.final_output.ptr as List<int>).Clear(); //to be on safe side..
				}	
				++this._idx;	
				
				if(this.CurrentIteration == this._currentDatasetSize-1){
					
						for(int m=0;m<10;++m){
							for(int j=0;j<10;++j){
								Console.Write (this._confusion_matrix[m,j].ToString("D4")+"\t");
							}
							Console.WriteLine();
						}

						Globals.Logger.Log ("Accuracy = " +
							((((float)this._predicted_correctly)/((float)this._size_calculated)).ToString()));					
					
				}
				
			}	
				

				
			this.CurrentIteration = (this.CurrentIteration+1)%this._currentDatasetSize;
			if(this.CurrentIteration == 0) this.CurrentBatch+=1;
			
			if(this.CurrentBatch == casted_parameters.BatchesToTrain){
				//note now it is very naive, just train N batches..
				foreach (Layer l in casted_parameters.Architecture) {
					l.Reset ();
				}	
				this._dflow.SetGlobalVariable("batch_size",1);
				this._precheck_dlow();
				this.State = NodeState.Ready; //TODO: this should be done by dflow
			}
		}
		
		
		
		
		
		#region DFlow.TrainableNode implementations
		/// <summary>
		/// Used by DFlow.TrainableNode to determine if we can process futher node in normal DFlow mode
		/// </summary>
		public override bool IsTrainingDone ()
		{
			Parameters p = GetParameters() as Parameters;
			return this.CurrentBatch >= p.BatchesToTrain;
		}
		
		public override void TrainingFinalize ()
		{
			Globals.Logger.Log ("Training is finished");
		}
		#endregion DFlow.TrainableNode implementations
		
			
		
		public class Parameters : TrainableNode.Parameters{
			/// <summary>
			/// The architecture - layers in order of feedforward flow
			/// </summary>
			public Layer[] Architecture = null;
			/// <summary>
			/// The init_procedure - in what order call Init(). Normally it wil be backwards
			/// </summary>
			public Layer[] InitializationProcedure = null;
			
			/// <summary>
			/// How many full batches process (can be stochastic gradient etc., just in general)
			/// TODO: add other criterion, like "patience" from deeplearning.net
			/// </summary>
			public int BatchesToTrain = 400;
			
			/// <summary>
			/// The size of the batch, it will be processed as one on GPU and then modified weights.
			/// Very imporant feature 
			/// 
			/// *NOTE* it doesn't check if batch will fit into memory, sorry.. I know it should :)
			/// </summary>
			public int BatchSize = 1;
			
			/// <summary>
			/// The classification layer - if not set it will scan and look for itself. Then
			/// it will be the last layer that is not LayerDual
			/// </summary>

			
			/// <summary>
			/// <not implemented> how much from test dataset test on after each batch
			/// </summary>
			public double TrainPercentage = 0.75; 
							
			public int ReportFrequency = 25000;
			
			public Parameters() : base(){
				this.persistent = true ; // no support for bson saving with gpu buffers, something worth doing
				this.storageurl = "neural_network.data";
			}
		}
		
		
		#region Fields
		private Engine _engine; 
		private Dataflow _dflow;
		public bool Initialized =false;
		public string Descriptor = "";
		
		[DFlow.OutputPin()]
		public object result;
				
		public FinalLayer FinalLayer = null;
		public int CurrentIteration = 0;
		public int CurrentBatch = 0;
	    int _currentDatasetSize = 0;
		object _currentDataset = null; //this is only because Count is O(N).. 
		
		//some magic variables used in TrainingProcess()
		int _predicted_correctly, _predicted_incorrectly,
			_size_calculated, _idx; 
		//some magic variables used in TrainingProcess()
		int [,] _confusion_matrix = null;
		List<object> _ids;
		
		
		#endregion Fields
	}
}

