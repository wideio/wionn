from pylab import *
from numpy import *
import os, struct
from array import array as pyarray
from numpy import append, array, int8, uint8, zeros
import scipy

def read(digits, dataset = "training", path = "."):
    """
    Loads MNIST files into 3D numpy arrays

    Adapted from: http://abel.ee.ucla.edu/cvxopt/_downloads/mnist.py
    """

    if dataset is "training":
        fname_img = os.path.join(path, 'train-images.idx3-ubyte')
        fname_lbl = os.path.join(path, 'train-labels.idx1-ubyte')
    elif dataset is "testing":
        fname_img = os.path.join(path, 't10k-images.idx3-ubyte')
        fname_lbl = os.path.join(path, 't10k-labels.idx1-ubyte')
    else:
        raise ValueError, "dataset must be 'testing' or 'training'"

    flbl = open(fname_lbl, 'rb')
    magic_nr, size = struct.unpack(">II", flbl.read(8))
    lbl = pyarray("b", flbl.read())
    flbl.close()

    fimg = open(fname_img, 'rb')
    magic_nr, size, rows, cols = struct.unpack(">IIII", fimg.read(16))
    img = pyarray("B", fimg.read())
    fimg.close()

    ind = [k for k in xrange(size) if lbl[k] in digits]
    N = len(ind)

    images = zeros((N, rows, cols), dtype=uint8)
    labels = zeros((N, 1), dtype=int8)
    for i in xrange(len(ind)):
        images[i] = array(img[ ind[i]*rows*cols : (ind[i]+1)*rows*cols ]).reshape((rows, cols))
        labels[i] = lbl[ind[i]]

    return images, labels

def main():

    images, labels = read(xrange(10), 'training')
    images_test, labels_test = read(xrange(10), 'testing')
    f = figure()

#     f.add_subplot(2, 1, 1)
#     imshow(images.mean(axis=0), cmap=cm.gray)
#     f.add_subplot(2, 1, 2)
#     imshow(images[0,0:,0:],cmap= cm.gray)
#     show()
#
#
    #Make directories
    [os.system("mkdir -p ./MNIST/{0}".format(i)) for i in xrange(10)]

    #Write digits
    counter = 0
    counter_test = 0
    omit = 1
    for digit in xrange(images.shape[0]):
        print "Processed {0}".format(counter)
        if counter % omit ==0: scipy.misc.imsave("./MNIST/{0}/img{1}.png".format(labels[counter,0],counter), images[counter,0:,0:])
        counter += 1
    for digit in xrange(images_test.shape[0]):
        print "Processed {0}".format(counter)
        if counter % omit ==0: scipy.misc.imsave("./MNIST/{0}/img{1}.png".format(labels_test[counter_test,0],counter), images_test[counter_test,0:,0:])
        counter_test += 1
        counter += 1

if __name__ == "__main__":
    main()
