using System;

using DFlow;
using DFlowCore;

using WIO.ComputingToolkit;
using WIO.Utils;
using WIODataLanguage;
using WIODatasetLanguage;

using System.Linq;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using WIO.NN.Obj;

using NUnit.Framework;
namespace WIO.NN
{

	
	
	
	
	
	/// <summary>
	/// Multi class best layer - from K classes pick biggest output
	/// </summary>
	public class MultiClassBestSqrErrorLayer : FinalLayer{
		public static double MultiClassBestLayerProfiler = 0.0;
		
		#region PINS
		
		[DFlow.InputPin()]
		//delta_dash
		public WIO.NN.Obj.ObjBatchWIOMatrix input;
		[DFlow.OutputPin()]
		
		/// <summary> The last layer returns to dual -DE/DA(l-1) it allows for propagating different errors</summary>
		public WIO.NN.Obj.ObjBatchWIOMatrix output;
		
		public bool ComputeCostFunction = false;
		public float CostFunction;
		#endregion PINS

		
		public MultiClassBestSqrErrorLayer(){
			WIO.Utils.ProfilerManager.RegisterTimer("MultiClassBestSqrErrorLayer");
		}
		public override void Process ()
		{
			WIO.Utils.ProfilerManager.StartProfilerTimer("MultiClassBestSqrErrorLayer");
			
			WIOOpenCLMatrix input_matrix = (WIOOpenCLMatrix)input.ptr;
			WIOOpenCLMatrix output_matrix = (WIOOpenCLMatrix)this.output.ptr;
			

			Assert.AreEqual(Math.Max(1,(int)this.host_Dataflow.GetGlobalVariable("batch_size")),
			                input_matrix.GetLength(1));
			Assert.AreEqual(input_matrix.GetLength(1),output_matrix.GetLength(1));
			Assert.AreEqual(input_matrix.GetLength(0), output_matrix.GetLength(0));
			

			
			this.input.Ready = false;
			this.output.Ready = false;	
				
			
			//no argmax in linq?, i could do it by reduce.. (Aggregate)
			
			if(this.ClassificationPhase || this.ComputeCostFunction){ //assumes that it is one by one now. trivial to change it
				//Classify 
				List<int> final_list = new List<int>();
				for(int j=0;j<input_matrix.GetLength(1);++j){
					int argmax = -1;
					float max = -3000.0f;
					for (int i=0; i< input_matrix.GetLength(0); ++i) {
						if (input_matrix [i + 1, j+1] > max) {
							max = input_matrix [i + 1, j+1];
							argmax = i;
						}
					}
					final_list.Add (argmax);
				}
				final_output.ptr = final_list;
	
				//It is final layer, so it is the output of whole node
				this.input.Ready = false;
				this.output.Ready = false;	
				
				if(this.ComputeCostFunction){
					this.CostFunction = 0.0f;
					throw new NotImplementedException();
//					for(int j=0;j<input_matrix.GetLength(1);++j){
//						float [] tmp = new float[input_matrix.GetLength (0)];
//						int argmax = -1;
//						float max = -3000.0f;
//						for (int i=0; i< input_matrix.GetLength(0); ++i) {
//							if (input_matrix [i + 1, j+1] > max) {
//								max = input_matrix [i + 1, j+1];
//								argmax = i;
//							}
//							tmp [i] = input_matrix [i + 1, j+1];
//						}
//						final_list.Add (argmax);
//					}					
				}
			}
			if(!this.ClassificationPhase)
			{
				
				
				
				//FIXME: just a prototype
				for(int j=1;j<=Math.Max (1,(int)this.host_Dataflow.GetGlobalVariable("batch_size"));++j){
					for(int i=1;i<=input_matrix.GetLength(0);++i){
						output_matrix[i,j] =
							-(        ((((List<int>)this.meta.ptr)[j-1]+1==i) ? 1.0f : 0.0f) - 
								input_matrix[i,j]             );
					}
				}
				
				this.input.Ready = false;
				this.output.Ready = true;	
			
			}
			
		

			
			WIO.Utils.ProfilerManager.StopProfilerTimer("MultiClassBestSqrErrorLayer");
		}
		

			
		public override bool IsReady ()
		{
			//facililates control of blocking flow when doing batching etc in the input layer
			return this.input.Ready;
		}
	
		public override string GetDescriptor ()
		{
			return "MultiClassBestSqrErrorLayer"+(GetParameters() as Parameters).K;
		}
		
		public override void UpdateParameters (object o)
		{
			Parameters casted_parameters = (Parameters)o;
			
			this.parameters = casted_parameters;
			
			
			this.input = new ObjBatchWIOMatrix(ToolkitBackend.OPENCL, casted_parameters.K);
			this.output = new ObjBatchWIOMatrix(ToolkitBackend.OPENCL, casted_parameters.K);
		}
		
		public new class Parameters : Layer.Parameters{
			public int K = 0;
		}
	}
}

