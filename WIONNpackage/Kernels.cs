using System;

using DFlow;
using DFlowCore;

using WIO.ComputingToolkit;
using WIO.Utils;
using WIODataLanguage;
using WIODatasetLanguage;

using System.Linq;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Diagnostics;

using WIO.NN.Obj;

using OpenCL.Net;

using NUnit.Framework;
namespace WIO.NN.Kernels
{
	
	/// <summary>
	/// Holds sigmoid-specific kernels, that cannot be generalized
	/// </summary>
	public static class SigmoidKernels{
	
		/// <summary>
		/// Very specific kernel for accumulating gradient from batch using gradient descent back prop.
		/// In the future we can think how to generalize it nicely
		/// 
		/// It calculates gradient and makes average
		/// 
		/// </summary>
		/// <param name="W">weight matrix</param>
		/// <param name="b">WX+(b)</param>
		/// <param name="a"> a^{l+1} activation </param>
		/// <param name="a_previous"> a^{l} activation </param>
		/// <param name="delta_dash_next">delta_dash^{l+1}, dash stands for not having derivative in it for performance issues</param>
		public static void SigGradientDescentFull(WIOOpenCLMatrix
		                                                  W, WIOOpenCLMatrix b,
		                                                    WIOOpenCLMatrix a_previous,
		                                                    WIOOpenCLMatrix a,
		                                                    WIOOpenCLMatrix delta_dash_next,
		                                                    float learning_epsilon){


			if(W.GetTransposeState()==1) throw new NotImplementedException();
			if(b.GetTransposeState()==1) throw new NotImplementedException();

			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(a);
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(a_previous);
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(delta_dash_next);			
		
		  	Cl.ErrorCode error;				
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("sig_gradient_desc_full");

			Assert.IsTrue(a_previous.GetLength(0)/W.GetLength(1) == delta_dash_next.GetLength(0)/W.GetLength(0));
			Assert.IsTrue(a.GetLength(0) == delta_dash_next.GetLength(0));
			
			Assert.IsTrue(delta_dash_next.GetLength(0) == W.GetLength(0));
			Assert.IsTrue(a_previous.GetLength(0) == W.GetLength(1));
			Assert.IsTrue (delta_dash_next.GetLength(1) == a_previous.GetLength(1));
			
		
			int intPtrSize = 0;
            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
			IntPtr sizer = new IntPtr(intPtrSize);
			Cl.SetKernelArg(kernel,0, sizer, (W.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,1, sizer, (b.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,2, sizer, (a_previous.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,3, sizer, (a.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,4,  sizer,(delta_dash_next.GetDual () as OpenCLResourceBufferHandle<float>).BufferPtr);
			Cl.SetKernelArg(kernel,5, learning_epsilon);
			Cl.SetKernelArg(kernel,6, delta_dash_next.GetLength(1));

			IntPtr[] dims = new IntPtr[]{ new IntPtr(W.GetLength(0)), new IntPtr(W.GetLength(1)) };
		   // execute kernel
            error = Cl.EnqueueNDRangeKernelNullEvent(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, 
			                                dims ,null );
            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
			
			(W.GetDual () as OpenCLResourceBufferHandle<float>).SetChangeSwitch(true);
			(b.GetDual () as OpenCLResourceBufferHandle<float>).SetChangeSwitch(true);
		}			
		
	}
}

