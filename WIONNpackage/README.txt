NOTES ON WIO ALGEBRA:
*OpenCLBufferable*
===
It is a general concept I think we might use. It uses idea of invariants and consistency. Look up OpenCLBufferable interface 
			
			note: every operation on GPU with bufferable objects should follow this convention.
			first ensure consistency (invariant on start)
			at the end ensure consistency of the result (invariant on the end)
			with the assumption that no thread will ever modify the mid-results!
			
NOTE:
	* it achieved 90% on MNISt for now (it should get much more, it was only too slow). Multi Layered
	Perceptron should achieve >95-98% on MNISTS according to literature so I am not 100% percent
	confident gradients are correct, but should be
	* it achieved 100% on Gen dataset (100 epochs)
		
				
TODO:
	* automatic opencl code generation for functions. it is a good idea ! now i have to add
		new .cl file for each mapply.. 	ones.MApplySigmoid(); '=
		//make more generic (mapply can be easily generic, with {0} and string format..)
	* there might be error in _check_dflow if there are multiple pins
	* softmax layer became a little obsolete - repair it.
	* params in WIO.NN.Obj should be dicts not raw arrays..
	
TODO IMPORANT:
	* generalize OpenCL calls, the repetition of code is just unacceptable. do it when done with prototype phase
	* image buffers for read only access - it is the main bottleneck now - loading to GPU memory. We
	should devise some good caching ideas etc. Not sure about that yet - and not priority now.
	Performance is on good level, however there are still some bottlenecks to address.
	IDEA: traning MAINLY on data that fits to GPU memory, and rare exchanges : easy to code and will give
	lightspeed performance
	
	
	
General workflow:
1. implement sigmoid MLP -- DONE
2. check sigmoid MLP on MNIST and GenDataset -- DONE (partially, but should be fine)
3. implement batching system and test on GenDataset without 1-sized batch for sanity check -- DONE
4. deploy full batching system.. -- DONE
5. switch to unmanaged arrays -- DONE
6. implement caching in input layer -- DONE
7. implement persistenty system -- DONE
8. implement backprop in CPU check if the same -- PARTIALLY
9. implement command tool -- DONE
10. implement aggreagation -- DONE
11. get rid of using engine from dflow and fire dataflows manually for now
12. make it lightspeed fast : caching on GPU and all to GPU
13. find bug, or make sure gradient is correct


Workflow very general
1. Fix parameters in generic datasets


SANITY CHECK:
	with 0.08 and 1 batch gradient MLP should get 1 accuracy in batch 3 on gen dataset.
	
	
OMG:
	NVIDIA is constantly violating specfs of OpenCL. 2 Proofs:
	5.1 (Unavailable devices)

-------------------------

o If a device(s) becomes unavailable after a context and command-queues that use

this device(s) have been created and commands have been queued to them, the

implementation will fail with the CL_OUT_OF_RESOURCES error for further API

calls. The state of the commands enqueued so far is left undefined.



The application should destroy the objects associated with such a context and

re-query the available device list.

	2. EnqueueNDRangeKernel is BLOCKING and synchronous!...
	
	
	