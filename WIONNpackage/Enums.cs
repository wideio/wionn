using System;
namespace WIO.NN
{
	/// <summary>
	/// WIONN layer buffer format : useful for communicating between nodes.
	/// It specifies only WHAT THE LAYER WANT. So previous layer based on Layer TYPE (class) and
	/// on format knows how to prepare input for this node.
	/// </summary>
	public enum LayerInputFormat{

		/// <summary>
		/// The STANDARD 1d vector of neurons , so it requires any N dimensional input (in parameters)
		/// 
		/// 
		/// Matrix of weights if it is neuron layer
		/// <w11.......w1n> - matrix of weights , so input is a column vector and output is activation_func(W*X)
		/// 
		/// 
		/// 
		/// <wn1.......wnn>
		/// </summary>
		STANDARD1DVECT,
		
		/// <summary>
		/// Input node will convert bitmap to 1D vector (see code)
		/// </summary>
		BITMAPINPUTNODE
	}
	
		
	public enum ToolkitBackend{
		/// <summary>
		/// Only available backend for now. Anyway it is not well-generic coded so it doesn't matter for now. 
		/// CPU based algebra on some library should be added for platforms not supporting OpenCl.
		/// </summary>
		OPENCL 
	}

}

