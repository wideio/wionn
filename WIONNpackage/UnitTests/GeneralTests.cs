using System;
using WIO.Utils;
using NUnit.Framework;
namespace WIO.NN
{
	public static class GeneralTests
	{
		public static void TestUnmanagedArray(){
			FixedArray<float> fa = new FixedArray<float>(100);
			fa[10] = 10.0f;
			fa[99] = 2000.0f;
			Assert.AreEqual(fa[10],10.0f);
		
		}
	}
}

