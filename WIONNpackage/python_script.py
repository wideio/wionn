import subprocess
import xml.dom.minidom

def main():
    file_name = "WIONN.csproj"

    doc = xml.dom.minidom.parse(file_name)

    release_node = doc.childNodes[0].childNodes[5]


    print release_node.toprettyxml()

    release_node.getElementsByTagName("OutputPath")[0].childNodes[0].nodeValue = "./lib"

    doc.writexml(open(file_name+".mod","w"))

    p = subprocess.Popen(
        ['xbuild',file_name+".mod", '/p:Configuration=Release'])
    p.communicate()



if __name__=="__main__":
    main()
