using System;

using DFlow;
using DFlowCore;

using WIO.ComputingToolkit;
using WIO.Utils;
using WIODataLanguage;
using WIODatasetLanguage;

using System.Linq;
using System.Drawing;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using System.Runtime.InteropServices;
using WIO.NN.Obj;

using NUnit.Framework;
namespace WIO.NN
{


	public abstract class InputLayer : Layer{

	}

	//TODO: Extract it into abstract class, and add SetSourceDataset function to it.

	/// <summary>
	/// Every input that goes into WIONN, goes through this node, so it should check if it is the correct format.
	/// After that each node will have invariant that everything before was "fine"
	/// </summary>
	public class BitmapInputLayer : InputLayer
	{
		public static double InputLayerProfiler = 0.0;
		#region PINS
		[DFlow.OutputPin()]
		public ObjBitmap output;

		#endregion PINS
		

		
		public BitmapInputLayer(){
			this.LayerFormat = LayerInputFormat.STANDARD1DVECT;
		}
		
		public override void Process(){
			
			OpenCL.Net.Cl.Flush(ToolkitGlobals.DefaultOpenCLNode._cmdQueue);
			
			
			System.DateTime start = System.DateTime.Now;
			
			this.output =  new ObjBitmap(this.host_Dataflow.source_dataset.GetElement(this.host_Dataflow.data_address));
			this.meta = new ObjMeta();
			this.meta.ptr = (int)this.host_Dataflow.source_dataset.GetMetaByLabel(host_Dataflow.data_address,"Label");
		
			InputLayerProfiler += (System.DateTime.Now - start).TotalMilliseconds;
		}
		
	}
	
	
	
	/// <summary>
	/// Batching input layer - it outputs WIONNObjBitmapArray
	/// TODO: add here caching from one file for speed purposes
	/// 
	/// Stops flows until gets enough input
	/// </summary>
	public class BatchingImagesInputLayer : InputLayer
	{
		#region PINS
		[DFlow.OutputPin()]
		public WIO.NN.Obj.ObjBitmapArray output = null;
		public int Counter = 0;

		#endregion PINS
		
		
		public BatchingImagesInputLayer(){
			this.LayerFormat = LayerInputFormat.STANDARD1DVECT;
			this.output = new WIO.NN.Obj.ObjBitmapArray(ToolkitBackend.OPENCL);
			
			WIO.Utils.ProfilerManager.RegisterTimer("BatchingImagesInputLayer");
			
		}
		
		
		
		public override void Reset ()
		{
			this.Counter = 0;
			this.output.Ready = false;
		}
		
		/// <summary>
		/// Process this instance
		/// </summary>
		/// 
		/// <remarks>
		/// Note: 
		/// this is going to be the bottleneck, around
		/// 100ms spent here every iteration .. cache bitmaps? Future direction of caching
		/// system, not make ImageDataset do it (cache = false)
		/// </remarks>
		public override void Process(){
			WIO.Utils.ProfilerManager.StartProfilerTimer("BatchingImagesInputLayer");
			
			if(this.Counter == 0){
				this.output.ptr =(new List<System.Drawing.Bitmap>());
				this.output.Ready = false;
				this.meta.ptr = (new List<int>());
			}
						
			OpenCL.Net.Cl.Flush(ToolkitGlobals.DefaultOpenCLNode._cmdQueue);
			
			//TODO: add here caching , creating same bitmap over and over in fact
			Bitmap bmp = new System.Drawing.Bitmap(
				(System.Drawing.Image)this.host_Dataflow.source_dataset.GetElement(this.host_Dataflow.data_address)
				);
			
			
			(this.meta.ptr as List<int>).Add ( 
			      (int)this.host_Dataflow.source_dataset.GetMetaByLabel(host_Dataflow.data_address,"Label")
			);
		
			(this.output.ptr as List<System.Drawing.Bitmap>).Add (bmp);
			
			
			
			this.Counter+=1;
			if(this.Counter == Math.Max (1,(int)this.host_Dataflow.GetGlobalVariable("batch_size"))){
				this.output.Ready = true;
				//input always ready
				this.Counter = 0;
			}else{
				this.output.Ready = false; //make sure
			}
			
			WIO.Utils.ProfilerManager.StopProfilerTimer("BatchingImagesInputLayer");
		}
		
	}
	

	/// <summary>
	/// Optimized version of BatchingImagesInputLayer, separated for convention, 
	/// in the future BatchingImagesInputLayer will become obsolete
	/// </summary>
	public class BatchingImagesOptimizedInputLayer : InputLayer, IDisposable
	{
		#region PINS
		[DFlow.OutputPin()]
		public WIO.NN.Obj.ObjBitmapUnmanagedBatch1D output = null;
		public int Counter = 0;

		#endregion PINS
		
		
		public BatchingImagesOptimizedInputLayer(){
			this.LayerFormat = LayerInputFormat.STANDARD1DVECT;
			
			
			WIO.Utils.ProfilerManager.RegisterTimer("BatchingImagesInputLayer");
			
		}
		public override void UpdateParameters (object o)
		{
			Parameters casted_parameters = (Parameters)o;
			this.parameters = o;
			
			
			//it is obligatory to specify as much as we can in update parameters - it is one of mechanism
			//of ensuring that type conversion and all is preserved
			this.output = new WIO.NN.Obj.ObjBitmapUnmanagedBatch1D(casted_parameters.backend,
			                                                       casted_parameters.Width, 
			                                                       casted_parameters.Height, -1, "float",null);
			
			

		}
		public new class Parameters : InputLayer.Parameters{
			/// <summary>
			/// The weight of input pictures
			/// </summary>
			public int Width;
			/// <summary>
			/// The height of input pictures
			/// </summary>
			public int Height;
			
			/// <summary>
			/// Note: this feature is next to implement in DatasetLanguage
			/// This class will check if it has already cached dataset of this name on disk
			/// 
			/// By default no name, so no caching possible
			/// </summary>
			public string DatasetName = "";
		}
		
		public override string GetDescriptor ()
		{
			return "BatchingImagesOptimizedInputLayer"; 
		}
		
		public override void Reset ()
		{
			this.Counter = 0;
			if(this.output != null) this.output.Ready = false;
		}
		
		private Dictionary<object, IntPtr> _cachedBitmaps = new Dictionary<object, IntPtr>();
		
		public override object GetPersistentData ()
		{
//			List<Tuple<object,float[]>> _dumpDict = new List<Tuple<object,float[]>>();
//			foreach(object id in _cachedBitmaps.Keys){
//				_dumpDict.Add (new Tuple<object,float[]>(id, 
//					
//					(new FixedArray<float>(_cachedBitmaps[id],(GetParameters() as Parameters).Width*(GetParameters() as Parameters).Height)
//				                 
//				                 
//				 ).ToArray()));
//				System.Runtime.InteropServices.Marshal.FreeHGlobal(_cachedBitmaps[id]);
//			}
//			return _dumpDict;
			return "empty";
		}
		
		public override void LoadPersistentData (object data)
		{
//			List<Tuple<object,float[]>> _dumpDict = data as List<Tuple<object,float[]>>;
//			foreach(var x in _dumpDict){
//				this._cachedBitmaps[x.Item1] = (new FixedArray<float>(x.Item2)).arrayPtr;
//			}
		}
		
		
		//FIXME: too much raw pointer maigc in Process - extract some reusable functions.
		
		/// <summary>
		/// Process this instance
		/// </summary>
		/// 
		/// <remarks>
		/// Note: 
		/// this is going to be the bottleneck, around
		/// 100ms spent here every iteration .. cache bitmaps? Future direction of caching
		/// system, not make ImageDataset do it (cache = false)
		/// </remarks>
		public override void Process(){
			WIO.Utils.ProfilerManager.StartProfilerTimer("BatchingImagesInputLayer");
		
			if(this.Counter == 0){
				
				//prepare it for future processing
				if(this.output.ptr!=null){
					//garbage collector will take care of it because we are rewiring it
//					System.Runtime.InteropServices.Marshal.FreeHGlobal(
//						(IntPtr)(this.output.ptr));
				}
				this.output = new WIO.NN.Obj.ObjBitmapUnmanagedBatch1D(
					ToolkitBackend.OPENCL,
					(GetParameters() as Parameters).Width, (GetParameters() as Parameters).Height,
					
					Math.Max (1,(int)this.host_Dataflow.GetGlobalVariable("batch_size")),
					 "float",
					null
					);
				this.output.ptr = (new FixedArray<float>((GetParameters() as Parameters).Width*
					                                        (GetParameters() as Parameters).Height*
					                                       Math.Max (1,(int)this.host_Dataflow.GetGlobalVariable("batch_size")))).arrayPtr;
				this.output.Ready = false;
				this.meta.ptr = (new List<int>());
			}
					


			if(this._cachedBitmaps.ContainsKey(this.host_Dataflow.data_address)){
				
				unsafe{
					//do memcpy using kernel32.dll
					WIO.Utils.UtilityFunctions.CopyMemory( (IntPtr)(
							
											        		 ((float*)(IntPtr)this.output.ptr) + 
													 		 this.Counter*(
													 		 GetParameters() as Parameters).Width*(GetParameters() as Parameters).Height
						 								),
					                                     this._cachedBitmaps[this.host_Dataflow.data_address],
					                                     (uint)((GetParameters() as Parameters).Width*(GetParameters() as Parameters).Height*sizeof(float))
					                            );
				}
			}
			else{
				//Create bitmap
				Bitmap bmp = new System.Drawing.Bitmap(
					(System.Drawing.Image)this.host_Dataflow.source_dataset.GetElement(this.host_Dataflow.data_address)
					);
				
	
				//Copy bitmap data to (byte*) pointer
				unsafe{
					//creating on the flow "view" to part of our main array.
				    WIO.NN.Obj.Converters.ConvertBitmapTo1DUnmanaged(bmp,
				  		  new ObjBitmapUnmanaged1D( ToolkitBackend.OPENCL,
										 (int)(GetParameters() as BatchingImagesOptimizedInputLayer.Parameters).Width,
				                         (int)(GetParameters() as BatchingImagesOptimizedInputLayer.Parameters).Height,
					                         "float",
					                      (IntPtr)(
						
										         ((float*)(IntPtr)this.output.ptr) + 
												  this.Counter*(
												  GetParameters() as Parameters).Width*(GetParameters() as Parameters).Height
					 						)
									)
					);
					//create cache in memory
					IntPtr dest = Marshal.AllocHGlobal(sizeof(float)*(GetParameters() as Parameters).Width*(GetParameters() as Parameters).Height);
					//assign to dict of cached bitmaps
					this._cachedBitmaps[this.host_Dataflow.data_address] = dest;
					//do memcpy using kernel32.dll
					WIO.Utils.UtilityFunctions.CopyMemory(  this._cachedBitmaps[this.host_Dataflow.data_address], 
					                                      (IntPtr)(
													         ((float*)(IntPtr)this.output.ptr) + 
															  this.Counter*(
															  GetParameters() as Parameters).Width*(GetParameters() as Parameters).Height
						 									),
					                                      (uint)((GetParameters() as Parameters).Width*(GetParameters() as Parameters).Height*sizeof(float))
					                                    );	
					
				}
			}
			
			//Note : conversion has passed this test, we can move it to unit tests,
			//Data is correct 
			//Do simillar test for bson reading..
//			{
//				IntPtr ptr = (IntPtr)this.output.ptr ;
//				FixedArray<byte> fa= new FixedArray<byte>(ptr, (GetParameters() as Parameters).Width*
//						                                        (GetParameters() as Parameters).Height*
//				                                          (int)this.host_Dataflow.GetGlobalVariable("batch_size"));
//				byte[] dumped_array = fa.ToArray();
//				
//				float[,] test_array = WIO.ComputingToolkit.Utils.Float2DMatrixFromBitmap(bmp, 1);
//				
//				int img_size = (GetParameters() as Parameters).Width*
//					(GetParameters() as Parameters).Height;
//				
//				for(int i=0;i<test_array.GetLength(0);++i){
//					for(int j=0;j<test_array.GetLength(1);++j){
//						Assert.AreEqual(test_array[i,j], dumped_array[
//						    this.Counter*img_size + j*test_array.GetLength(1) + i]);
//						                                              
//					}
//				}
//			}	
		
				

			//Bookeeping			
			(this.meta.ptr as List<int>).Add ( 
			      (int)this.host_Dataflow.source_dataset.GetMetaByLabel(host_Dataflow.data_address,"Label")
			);		
			this.Counter+=1;
			
			
			
			if(this.Counter == ((Math.Max (1,(int)this.host_Dataflow.GetGlobalVariable("batch_size"))))){
				this.output.Ready = true;
				this.Counter = 0;
			}else{
				this.output.Ready = false; //make sure
			}
			
			WIO.Utils.ProfilerManager.StopProfilerTimer("BatchingImagesInputLayer");
		}
		
		
		bool _disposed = false;
		
		public void Dispose(){
			
			if(!_disposed){
				foreach(IntPtr unmanaged_ptr in this._cachedBitmaps.Values)
					Marshal.FreeHGlobal(unmanaged_ptr);
				_disposed = true;
			}
		}
		
	}	
	
	
	
	
}

