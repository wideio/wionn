//specific sigmoid backprop kernel
//it will be generalized in the future
__kernel void
SigGradientDescentFull(__global float * W,
__global float * B, 
const __global float * a_prev, 
const __global float * a, 
const __global float * delta_dash_next,
const float epsilon ,
const int batches
	)
{
    // Vector element index
    int i = get_global_id(0);
    int j = get_global_id(1);

    int p = get_global_size(0);
    int r = get_global_size(1);

	

	//Calculate W
	for(int b=0;b<batches;++b)
		W[get_index2(i,j,p,0)] -= (epsilon/(float)batches)*(delta_dash_next[get_index2(i,b,p,0)])*a_prev[get_index2(j,b,r,0)];

	//Change b
	if(j==0)
		for(int b=0;b<batches;++b)
			B[i] -= (epsilon/(float)batches)*(delta_dash_next[get_index2(i,b,p,0)]);
	
}
