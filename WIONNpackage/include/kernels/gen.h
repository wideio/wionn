//returns computed index when given index order//
int get_index2(int row, int col, int rows_dev, int trans){
    if(trans == 0) return col*rows_dev + row;
    else return row*rows_dev + col;
}
//if we do not know dev rows/cols
//not f overloading in C99  iso..
int get_index2full(int row, int col, int rows, int cols, int trans){
    if(trans == 0) return col*rows + row;
    else return row*cols + col;
}


//for tran=0 we got column-wise order not row-wise!
