using System;


using WIO.ComputingToolkit;
using WIO.Utils;
using WIODataLanguage;
using WIODatasetLanguage;

using DFlow;
using DFlowCore;

using System.IO;
using System.Linq;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using MongoDB.Bson;
using MongoDB.Driver;

using NUnit.Framework;

namespace WIO.NN
{
	public abstract class AggregatorNode : DFlow.Node{
		[DFlow.OutputPin()]
		public List<Tuple<string,object>> output = null;
		public override void Process ()
		{
			output = new  List<Tuple<string,object>> ();
			foreach(var input_pin in this.ListInputPins())
				output.Add (new Tuple<string,object>(input_pin.Name,  input_pin.GetValue(this)));
		}
		
		public AggregatorNode(){
			this.State = NodeState.Ready;
			
			
		}
		
		public class Parameters{
		}
	}
	
				
	public class AggregatorSingleNode : AggregatorNode{
		[DFlow.InputPin()]
		public object input; //there should be in dflow methd to point multiple wires to one input which has Add option
	
		
		
		public override bool IsReady ()
		{
			return this.input != null;
		}
	
		public override void Process(){
			base.Process();
			this.input = null;
		}
	}
	
	/// <summary>
	/// Aggregate node - it collects or inputs at writes them to MongoDB database,
	/// Which can be afterwards read by MongoDB dataset.
	/// </summary>
	public class WriteMongoNode : DFlow.Node
	{
		/// <summary>
		/// The input - this input is going to be done by aggregator node
		/// </summary>
		[DFlow.InputPin()]
		public List<Tuple<string, object>> input; 
		
		
		public MongoClient client = new MongoClient(); //every dataset will establish its own connection
		MongoServer server;
		MongoDatabase db;

		
		public WriteMongoNode(){
			this.State = NodeState.Ready;
		}
		
		public override void UpdateParameters (object o)
		{
			base.UpdateParameters (o);
			this.parameters = o;
			Parameters casted_parameters = (this.parameters) as Parameters; 
			
			
			this.server = client.GetServer(); //now it works locally TODO: check where to set external database
			this.db = server.GetDatabase(casted_parameters.TargetDatabase);
			if(this.db.CollectionExists(casted_parameters.TargetCollection))
			     this.db.DropCollection(casted_parameters.TargetCollection);
			this.db.CreateCollection(casted_parameters.TargetCollection);
		}
		
		
		public override bool IsReady ()
		{
			return this.input != null;
		}
	
	
		public override void Process ()
		{
			Parameters casted_parameters = (this.parameters) as Parameters; 
		
			
			MongoCollection<BsonDocument> col = this.db.GetCollection(casted_parameters.TargetCollection);
			BsonDocument to_insert = new BsonDocument();
			foreach(var tuple in input){
				to_insert[tuple.Item1] = DFlowCore.BsonUtils.BsonEncode(tuple.Item2,tuple.Item2.GetType());
			}
			
			col.Insert(to_insert);
			
			this.input = null;
		}
		
		public class Parameters{
			public string TargetDatabase="";
			public string TargetCollection="";
		}
	}
	
	
	/// <summary>
	/// Aggregate node - it collects or inputs at writes them to MongoDB database,
	/// Which can be afterwards read by MongoDB dataset.
	/// </summary>
	public class WriteToDiskNode : DFlow.Node
	{
		/// <summary>
		/// The input - this input is going to be done by aggregator node
		/// </summary>
		[DFlow.InputPin()]
		public List<Tuple<string, object>> input = null; 
		public int ItemCount = 0 ;

		
		public WriteToDiskNode(){
			this.State = NodeState.Ready;
		}
		
		public override void UpdateParameters (object o)
		{
			base.UpdateParameters (o);
			this.parameters = o;
			Parameters casted_parameters = (this.parameters) as Parameters; 
			
			if(!casted_parameters.StorageUrl.EndsWith("/") && !casted_parameters.OneFile) casted_parameters.StorageUrl+="/";
			this.ItemCount = 0;
			
			if(casted_parameters.OneFile){
				
				current_stream=File.Open(
					casted_parameters.StorageUrl, FileMode.Create);
				this.summary_document = new BsonDocument();
			}
		}
		
		FileStream current_stream;
		
		public override bool IsReady ()
		{
			return this.input != null && input.Count != 0;
		}
	
	
		public override void Process ()
		{
			if(this.host_Dataflow.source_dataset != this.current_dataset){
				this.current_dataset = this.host_Dataflow.source_dataset;
				this.current_dataset_count = this.current_dataset.Count;
			}
			
			this.ItemCount+=1;
			
			Parameters casted_parameters = (this.parameters) as Parameters; 
			BsonDocument to_insert = new BsonDocument();
			foreach(var tuple in input){
					to_insert[tuple.Item1] = DFlowCore.BsonUtils.BsonEncode(tuple.Item2,null);
			}		
			
			if(casted_parameters.OneFile){
				
				this.summary_document[this.ItemCount.ToString()] = to_insert;
				
				
				if(this.ItemCount == this.current_dataset_count){
					byte[] b = DFlowCore.BsonUtils.
					Encode(new BsonDocument("data",
					                this.summary_document), maximum_size :160000000);	
					this.current_stream.Write (b,0,b.Length);
					this.current_stream.Close ();
				}
				
			}
			else{

					
	 
					byte[] b = DFlowCore.BsonUtils.
					Encode(new BsonDocument("data",
					                to_insert), maximum_size :160000000);		
				
					FileStream current_stream_local = File.Open(
					casted_parameters.StorageUrl+this.ItemCount.ToString()+".data", FileMode.Create);
				
					current_stream_local.Write(b, 0, b.Length);	
					current_stream_local.Close();
				
			}
			
	

			
			

			this.input = null;
		}
		
		public IDataset current_dataset;
		public int current_dataset_count;
		BsonDocument summary_document;
		
		public class Parameters{
			/// <summary>
			/// If set to false, it will write each output to separate file.
			/// </summary>
			public bool OneFile = true;
			public string StorageUrl = "./output.data";
		}
	}	
	
}

