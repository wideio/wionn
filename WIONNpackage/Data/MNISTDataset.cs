using System;

using WIODataLanguage;
using WIODatasetLanguage;

using OpenCL.Net;

using WIO.ComputingToolkit;

using System.Drawing;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using DFlow;
using DFlowCore;

using Emgu;
using Emgu.CV;
namespace WIO.NN
{
	[WIODataLanguage.WIOCallnameAnnotation("MNISTDataset")]
	public class MNISTDataset : ImageDataset
	{
		public int GetMetadataLabel(object id){
			string[] tokens = ((string)id).Split ('/');
			return (int)( (char)tokens[tokens.Length-2][0] - '0');
			
			
		}
	}
}

