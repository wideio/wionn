using System;
using WIODataLanguage;
using WIODatasetLanguage;
using System.Drawing;
using System.Drawing.Text;
using System.Collections;
namespace WIO.NN
{
	public class GenLetterDataset: IDataset
	{
		private new Parameters parameters;
		
		public class Parameters
		{
			public string fontname = "Ubuntu";
			public int sz = 32;		
		}

		public override void UpdateParameters(object p){
			Parameters casted_parameters = (Parameters)p;
			this.parameters = new Parameters{fontname = casted_parameters.fontname, sz = casted_parameters.sz};
			
		}

	
		public override IEnumerable Keys ()
		{	
			for(char letter = 'A'; letter <= 'Z'; letter++ ) 
			{
				yield return (object)letter.ToString ();			
			}
		}

		public bool checkFont ()
		{
			InstalledFontCollection installedFontCollection = new InstalledFontCollection();
			FontFamily[] families = installedFontCollection.Families;
			for (int i = 0; i < families.Length; i++) {
			
				if(families[i].Name.Equals(this.parameters.fontname)){
					return true;
				}
			}
			return false;
		}

		public void printFontList ()
		{
			InstalledFontCollection installedFontCollection = new InstalledFontCollection();
			FontFamily[] families = installedFontCollection.Families;
			for (int i = 0; i < families.Length; i++) {
				Console.WriteLine(families[i].Name);

			}
	
		}

	    [DatasetType(typeof(System.Drawing.Image))]     
	    public override object GetElement(object id) {
			char c = ((string)id)[0];

			if(!this.checkFont()) return null;

			Font font = new Font(this.parameters.fontname, this.parameters.sz);

			Image img = new Bitmap(1, 1);
			Graphics g = Graphics.FromImage(img);
			SizeF textSize = g.MeasureString(c.ToString(),font);
			img.Dispose();
			g.Dispose();

			img = new Bitmap((int) textSize.Width, (int)textSize.Height);
			g = Graphics.FromImage(img);
			g.Clear(Color.White);

			Brush textBrush = new SolidBrush(Color.Black);
			g.DrawString(c.ToString(), font, textBrush, 0, 0);

			g.Save();

			//img.Save(c + ".png", ImageFormat.Png);

			g.Dispose();
			textBrush.Dispose();
		
			return (object)img;
		}


		[DatasetType(typeof(int))]
		public object GetMetadataLabel(object id) {
			//Parameters p = GetParameters() as Parameters;
			/*// IMAGE METADATA
			System.Drawing.Imaging.PropertyItem[] propItems = Image.FromFile(p.pathToImg + filesDict[(int)id]).PropertyItems;
			return (object)propItems;*/
			return ((string)id)[0] - 'A';
		}  
	}
}

